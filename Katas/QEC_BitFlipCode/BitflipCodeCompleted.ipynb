{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Quantum Error-Correcting Codes\n",
    "\n",
    "Quantum states are fragile, being easily disturbed by minute interactions with the environment. If we are to use quantum states to process or transmit information, it is important to be able to design ways of storing information robustly, so that even if a state undergoes some amount of change, the information it contains is still recoverable. Clasically, error-correcting codes involve encoding information **redundantly**. A challenge to this in for quantum information processing is the **no cloning theorem**, which states that one cannot simply make a copy of an arbitrary quantum state. Moreover, even if this were possible, copies could not be measured independently and compared. \n",
    "\n",
    "We will look at the **three qubit bit flip code**, which proposes a way of storing one qubit of information resiliently in three qubits, so that even if the state is affected by a single error, in any of the three qubits, the error can be detected and the original state of the qubit recovered.\n",
    "\n",
    "More specifically, the code we will describe is robust to single **bit flip errors**. The model we imagine is that qubits are transmitted through a channel that leaves their state untouched with probability $1-p$, and flips the qubit (i.e. performs an `X` operation) with complementary probability $p$.\n",
    "\n",
    "To protect against such errors, we encode the single qubit $|\\psi\\rangle = a|0\\rangle + b|1\\rangle$ as the state $a|000\\rangle + b|111\\rangle$. When encoding information into qubits, one usually draws a distinction between **physical** qubits in the machine, and **logical qubits** as pieces of information. Here we use the physical basis states $|000\\rangle$ and $|111\\rangle$ to represent the logical basis states $|0\\rangle$ and $|1\\rangle$. It will often be convenient to denote these logical states as $|0_L \\rangle$ and $ |1_L \\rangle $.\n",
    "\n",
    "\n",
    "The tasks below in cells contaiing the `%kata` command are adapted from the Microsoft Quantum Katas in Q#. When the cell is run, unit tests are performed to check the correctness of the code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"Microsoft.Quantum.Standard::0.6.1905.301\",\"Microsoft.Quantum.Katas::0.6.1905.301\"]",
      "text/html": [
       "<ul><li>Microsoft.Quantum.Standard::0.6.1905.301</li><li>Microsoft.Quantum.Katas::0.6.1905.301</li></ul>"
      ],
      "text/plain": [
       "Microsoft.Quantum.Standard::0.6.1905.301, Microsoft.Quantum.Katas::0.6.1905.301"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%package Microsoft.Quantum.Katas::0.6.1905.301"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Measuring Parity\n",
    "\n",
    "The **parity** of a computational basis state is defined as a bitwise sum, i.e. for the state $ | x_1 x_2 \\ldots x_n\\rangle$, with $x_i \\in \\{0,1 \\}$, the parity is $ x_1 \\oplus \\ldots \\oplus x_n $. So $|000\\rangle$ and $|110\\rangle$ have $0$ (even) parity, whereas  $|100\\rangle$ and $|111\\rangle$ have $1$ (odd) parity.\n",
    "\n",
    "For a basis state, parity can be determined by performing a *joint* measurement in the computational basis. One way of understanding this is as follows. The computational basis consists of the two eigenvectors of the Pauli Z matrix given by\n",
    "\n",
    "$$\n",
    "Z = \\begin{pmatrix}\n",
    "1 & 0 \\\\\n",
    "0 & -1 \n",
    "\\end{pmatrix}.\n",
    "$$ \n",
    "\n",
    "The two eigenvectors are \n",
    "$ |0\\rangle = \\begin{pmatrix}\n",
    "1 \\\\\n",
    "0\n",
    "\\end{pmatrix}$\n",
    "with eigenvalue $1$ and $ |1\\rangle = \\begin{pmatrix}\n",
    "0 \\\\\n",
    "1\n",
    "\\end{pmatrix}$ with eigenvalue $-1$. We will confirm this below by performing measurements. Note that as always, new qubits are initialized in the `Zero` state.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "operation MeasureEven(): Result {\n",
    "    using(q=Qubit()){\n",
    "        let output= M(q);\n",
    "        Reset(q);\n",
    "        return(output);\n",
    "    }\n",
    "}\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%simulate MeasureEven"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So the measurement matches the parity of this simple state. Similarly for the state $|1\\rangle$, which is odd:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "operation MeasureOdd(): Result {\n",
    "    using(q=Qubit()){\n",
    "        X(q);\n",
    "        let output= M(q);\n",
    "        Reset(q);\n",
    "        return(output);\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%simulate MeasureOdd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So measuring in the $Z$ basis gives the parity of each single qubit basis state.\n",
    "\n",
    "For systems of multiple qubits, joint measurement in the $Z$ basis depends entirely on the parity of the measured state. To see this, note that the matrix representing the joint measurement $Z \\otimes Z$ is\n",
    "\n",
    "$$\n",
    "Z \\otimes Z = \\begin{pmatrix}\n",
    "1 & 0 & 0 & 0 \\\\\n",
    "0 & -1 & 0 & 0 \\\\\n",
    "0 & 0 & -1 & 0 \\\\\n",
    "0 & 0 & 0 & 1 \n",
    "\\end{pmatrix}.\n",
    "$$ \n",
    "\n",
    "The four basis states are eigenvectors for this matrix:\n",
    "$$(Z \\otimes Z) |00\\rangle = |00\\rangle $$\n",
    "\n",
    "$$(Z \\otimes Z) |01\\rangle = -|01\\rangle $$\n",
    "\n",
    "$$(Z \\otimes Z) |10\\rangle = -|10\\rangle $$\n",
    "\n",
    "$$(Z \\otimes Z) |11\\rangle = |11\\rangle. $$\n",
    "\n",
    "\n",
    "As in the single qubit case, the output of the measurement is $\\frac{1}{2} \\left(1-(-1)^{\\text{parity}}\\right)$. We check this for the state $|01\\rangle$ in the code below\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"MeasTwo\"]",
      "text/html": [
       "<ul><li>MeasTwo</li></ul>"
      ],
      "text/plain": [
       "MeasTwo"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "operation MeasTwo(): Result {\n",
    "     using(q=Qubit[2]){\n",
    "        X(q[1]);\n",
    "        let output = Measure([PauliZ,PauliZ],q);\n",
    "        ResetAll(q);\n",
    "        return output;\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "1",
      "text/plain": [
       "One"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%simulate MeasTwo"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise**: Check that measuring `PauliZ` recovers the parity of the other three basis ststes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task 1** \n",
    "\n",
    "You are given a three qubit register, which is either in an unknown basis state or in a superposition of basis states with the same parity. Write an operation that outputs the parity of the state, encoded as a value of the type `Result`, with `Zero` representing parity $0$. Your operation should not change the state of the qubits. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T01_MeasureParity_Test\n",
    "operation MeasureParity (register : Qubit[]) : Result {\n",
    "    \n",
    "        return Measure([PauliZ,PauliZ,PauliZ],register);\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question**: How do we know that the measurement did not change the state of the qubit?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task 2**\n",
    "\n",
    "You are given a register of three qubits, prepared in the state $|\\psi\\rangle|00\\rangle$ where $|\\psi\\rangle = \\alpha |0\\rangle + \\beta|1\\rangle$ is a message to be encoded.\n",
    "\n",
    "Write an operation that transforms this state to the encoded state\n",
    "\n",
    "$$ |\\psi_L \\rangle = \\alpha|000\\rangle + \\beta |111 \\rangle.  $$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T02_Encode_Test\n",
    "operation Encode (register : Qubit[]) : Unit {\n",
    "        CNOT(register[0],register[1]);\n",
    "        CNOT(register[0],register[2]);\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Elementary error detection   \n",
    "  \n",
    "Now, the input qubit is either in the state $|\\psi_L\\rangle$ above, or in an error state caused by a bit flip on the leftmost qubit i.e. $X \\otimes I \\otimes I |\\psi_L\\rangle = \\alpha|100\\rangle + \\beta |011 \\rangle $.\n",
    " \n",
    "Write an operation that outputs `Zero` if the state is $|\\psi_L\\rangle$ (i.e. no error) and `One` if the state is $X|\\psi\\rangle$ (i.e. an error has occurred).\n",
    "\n",
    "After applying the operation, the state of the input qubit should be unchanged."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T03_DetectErrorOnLeftQubit_Test\n",
    "operation DetectErrorOnLeftQubit (register : Qubit[]) : Result {\n",
    "    let res = Measure([PauliZ,PauliZ],register[0..1]);\n",
    "    return res;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, for the same input as described above, the aim is to **correct** the error if it is present (and otherwise do nothing)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T04_CorrectErrorOnLeftQubit_Test\n",
    "operation CorrectErrorOnLeftQubit (register : Qubit[]) : Unit {\n",
    "    \n",
    "    let res = Measure([PauliZ,PauliZ],register[0..1]);\n",
    "    if(res==One){\n",
    "        X(register[0]);\n",
    "    }\n",
    "    \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T04_CorrectErrorOnLeftQubit_Test\n",
    "operation CorrectErrorOnLeftQubit (register : Qubit[]) : Unit {\n",
    "    CNOT(register[1],register[0]);\n",
    "    using(ancilla=Qubit()){\n",
    "        CNOT(register[0],ancilla);\n",
    "        CNOT(register[1],register[0]);\n",
    "        CNOT(ancilla,register[0]);\n",
    "        Reset(ancilla);\n",
    "    } \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results above assumed we knew the bit to which any error had been applied, which was not realistic. In the next task, we aim to detect a bit flip error in *any* of the three qubits. The input qubit is either in the state $ |\\psi_L \\rangle = \\alpha|000\\rangle + \\beta |111 \\rangle $ or in one of the three possible single bit flip error states, i.e. $X \\otimes I \\otimes I |\\psi_L\\rangle = \\alpha|100\\rangle + \\beta |011 \\rangle $, $I \\otimes X \\otimes I |\\psi_L\\rangle $, or $I \\otimes I \\otimes X |\\psi_L\\rangle $.\n",
    "\n",
    "Your task is to write an operation that specifies which error, if any, has occurred. The operation should output $0$ if no error occurs, and the position (1, 2 or 3) of the error if any is present.\n",
    "\n",
    "After applying the operation the state of the qubits should not change."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T05_DetectErrorOnAnyQubit_Test\n",
    "\n",
    "operation DetectErrorOnAnyQubit (register : Qubit[]) : Int {\n",
    "    mutable output=0;\n",
    "    let res1 = Measure([PauliZ,PauliZ],register[0..1]);\n",
    "    let res2 = Measure([PauliZ,PauliZ],register[1..2]);\n",
    "    if(res1==One and res2 == Zero){\n",
    "        set output = 1;\n",
    "    }\n",
    "    if(res1==Zero and res2 == One){\n",
    "        set output = 3;\n",
    "    }\n",
    "    \n",
    "    if(res1==One and res2 == One){\n",
    "        set output = 2;\n",
    "    }\n",
    "    \n",
    "    return output;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Logical (as opposed to physical) quantum gates\n",
    "\n",
    "The aim of this task is to design a **logical** `X` gate, i.e. an operation $X_L$ that acts on a physical three qubit register encoding a single logical qubit such that $X_L |0_L\\rangle = |1_L\\rangle$ and $X_L |1_L\\rangle = |0_L\\rangle$.\n",
    "\n",
    "As before, the input qubit is either in the state $ |\\psi_L \\rangle = \\alpha|000\\rangle + \\beta |111 \\rangle $ or in one of the three possible single bit flip error states. If the state has an error, you can fix it, but this is not required."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T07_LogicalX_Test\n",
    "operation LogicalX (register : Qubit[]) : Unit {\n",
    "    for(i in 0..2){\n",
    "        X(register[i]);\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An alternative is to use `ApplyToEach` as follows"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T07_LogicalX_Test\n",
    "operation LogicalX (register : Qubit[]) : Unit {\n",
    "    ApplyToEach(X,register);\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, the aim of this task is to design a **logical** `Z` gate, i.e. an operation $Z_L$ that acts on a physical three qubit register encoding a single logical qubit such that $Z_L |0_L\\rangle = |0_L\\rangle$ and $X_L |1_L\\rangle = -|1_L\\rangle$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T08_LogicalZ_Test\n",
    "    operation LogicalZ (register : Qubit[]) : Unit {\n",
    "        ApplyToEach(Z,register);\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Further reading\n",
    "\n",
    "Quantum Error Correction is a very big topic, and an active research area. To find out more, a good place to start would be Chapter 10 of the book by Nielsen and Chuang. Deep work on **surface codes** is covered in the review article by [Fujii](https://arxiv.org/pdf/1504.01444.pdf)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Q#",
   "language": "qsharp",
   "name": "iqsharp"
  },
  "language_info": {
   "file_extension": ".qs",
   "mimetype": "text/x-qsharp",
   "name": "qsharp",
   "version": "0.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
