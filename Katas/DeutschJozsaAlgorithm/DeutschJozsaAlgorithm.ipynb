{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The Deutsch-Jozsa Algorithm\n",
    "\n",
    "In this notebook, we will consider the first quantum algorithm that was shown to improve performance over any classical algorithm for the same task. The task itself is rather artificial, but illustrates well how the principle of superposition can be used to speed up computation.\n",
    "\n",
    "Consider a function $f$ with an $n$-bit binary input and a $1$-bit binary output, so $f:\\{0,1\\}^n \\rightarrow \\{0,1\\}$. To give some examples, suppose we take $n = 2$. One such function is\n",
    "\n",
    "$$ f(0,0) = f(1,0) = 0, \\qquad f(0,1) = f(1,1) = 1; $$\n",
    "\n",
    "another is \n",
    "\n",
    "$$ f(0,0) = f(1,0) = f(0,1) = f(1,1) = 1. $$\n",
    "\n",
    "\n",
    "**Question**: how many such functions are there for $n=2$? For general $n$?\n",
    "\n",
    "The first function above is an example of a **balanced** function: it takes the two binary output values $0$ and $1$ for precisely the same number of inputs. Here, two inputs map to $0$ and two map to $1$. The second function is **constant**, assuming only one value. The **Deutsch-Jozsa problem**  asks how efficiently we can discriminate between balanced and constant functions. More precisely: if we know that a function $f$ is either **balanced** or **constant**, how many times to we need to evaluate $f$ to determine which of the two cases we are in?\n",
    "\n",
    "**Question**: For an $n$-bit function $f$ that is either balanced or constant (but otherwise arbitrary), how would you determine whether it is constant or balanced? How many times do you need to evaluate $f$ in the worst case?\n",
    "\n",
    "David Deutsch showed in 1992 that a quantum circuit exists that requires only a single evaluation of $f$ (or more accurately, only a single call to an **oracle** that implements $f$.\n",
    "\n",
    "## Deutsch's problem - the case $n=1$\n",
    "\n",
    "To present the ideas behind the algorithm clearly, we begin by considering the $n=1$ case that Deutsch originally worked on. In this case, there are precisely four functions \n",
    "\n",
    "$$ f_1(0) = f_1(1) = 0 \\qquad f_2(0)=0, f_2(1) =1 \\qquad f_3(0)=1, f_3(1) =0 \\qquad f_4(0) = f_4(1) =1. $$\n",
    "\n",
    "So for $n=1$, *any* function is either balanced or constant. Clearly we need to evaluate $f$ on each of the two possible inputs to determine which case we are in.\n",
    "\n",
    "What Deutsch showed is that **quantum parallelism** gives us access to global information about $f$, in this case whether it is constant or balanced, without evaluating $f(0)$ or $f(1)$ explicitly.\n",
    "\n",
    "## Unitary oracles for $f$\n",
    "\n",
    "Quantum circuits work exclusively with gates that represent **unitary** operators, i.e. operators $U$ for which $U^\\dagger U = UU^\\dagger = I,$ where $U^\\dagger$ is the **hermitian adjoint** of $U$. We form $U^\\dagger$ by taking the conjugate transpose - e.g. for the $2\\times2$ complex matrix\n",
    "\n",
    "\\begin{equation*}\n",
    "U =\\begin{pmatrix}\n",
    "a &  b  \\\\\n",
    "c &  d\n",
    "\\end{pmatrix},\n",
    "\\end{equation*}\n",
    "\n",
    "we have \n",
    "\n",
    "\\begin{equation*}\n",
    "U^\\dagger =\\begin{pmatrix}\n",
    "\\bar{a} &  \\bar{c}  \\\\\n",
    "\\bar{b} &  \\bar{d}\n",
    "\\end{pmatrix}.\n",
    "\\end{equation*}\n",
    "\n",
    "(Recall that if $z = x+ iy$ is a complex number, its complex conjugate $\\bar{z}$ is given by $x-iy$.)\n",
    "\n",
    "In particular, this means that quantum gates must be invertible, since $U^\\dagger$ is the matrix inverse of $U$.\n",
    "\n",
    "This presents a difficulty when representing some functions $f$ in the Deutsch-Jozsa problem. Certainly, constant functions are not invertible. The trick is to produce a **unitary oracle** on a larger qubit register.\n",
    "\n",
    "Sticking with the $n=1$ case for the moment, we work with two qubits, $x$ and $y$. Define $U_f |x,y\\rangle = |x, y\\oplus f(x)\\rangle$. \n",
    "\n",
    "e.g. for $f(x) = x$, we have\n",
    "\n",
    "    \n",
    "\n",
    "| $x$ | $y$ | $y \\oplus f(x)$ |\n",
    "|-----|-----|:---------------:|\n",
    "| 0   | 0   |       $0$       |\n",
    "| 0   | 1   | $1$             |\n",
    "| 1   | 0   | $1$             |\n",
    "| 1   | 1   | $0$             |\n",
    "\n",
    "This gives a unitary operator that evaluates $f$, and so can be embedded in a quantum circuit. Of course, for different functions $f$, we would need a different oracle. For this choice of $f$, $U_f$ can be represented by a `CNOT` gate.\n",
    "\n",
    "\n",
    "For a state $|x\\rangle$, define $U_f |x\\rangle = (-1)^{f(x)} |x\\rangle$. This means that the two computational basis states $|0\\rangle$ and $|1\\rangle$ only undergo (at most) a global phase change, i.e. no measurable change at all. However, other states are affected differently. Consider for example the state $ |+\\rangle = \\frac{1}{\\sqrt{2}}\\left( |0\\rangle + |1\\rangle\\right) $. \n",
    "\n",
    "$$ U_f |+\\rangle = (-1)^{f(0)}|0\\rangle + (-1)^{f(1)}|1\\rangle = (-1)^{f(0)}\\left(|0\\rangle + (-1)^{f(1)-f(0)}|1\\rangle\\right) .$$\n",
    "\n",
    "As before the *global* phase factor $(-1)^{f(0)}$ makes no measurable difference to the state, so the state $U_f |+\\rangle $ is determined by the *relative* phase factor $ (-1)^{f(1)-f(0)}$, which is $-1$ if the function is balanced, and $1$ if the function is constant.\n",
    "\n",
    "This observation suggests a way to discriminate between balanced and constant functions. First, take two qubits, one prepared in the $0\\rangle$ state, and one in the $1\\rangle\\$ state. Apply a Hadamard gate to both qubits. The state of the system is then\n",
    "\n",
    "$$H\\otimes H|01\\rangle =  \\frac{1}{2} \\left(|0\\rangle + |1\\rangle\\right)\\left(|0\\rangle - |1\\rangle\\right). $$\n",
    "\n",
    "We now apply the oracle $U_f$, obtaining\n",
    "\n",
    "\\begin{align} \\begin{cases}\n",
    "\\left(|0\\rangle + |1\\rangle\\right) \\left(|0\\rangle - |1\\rangle\\right) \\quad f(0) = f(1) \\\\\n",
    "\\left(|0\\rangle - |1\\rangle\\right) \\left(|0\\rangle - |1\\rangle\\right) \\quad f(0) \\neq f(1). \\\\\n",
    "\\end{cases}\n",
    "\\end{align}\n",
    "\n",
    "\n",
    "If we now apply a Hadamard gate to the first qubit, its state will be\n",
    "\n",
    "\\begin{align} \\begin{cases}\n",
    "0\\rangle \\quad f(0) = f(1) \\\\\n",
    "1\\rangle \\quad f(0) \\neq f(1). \\\\\n",
    "\\end{cases}\n",
    "\\end{align}\n",
    "\n",
    "So now, measuring in the computational basis gives a different state for the two cases, constant and balanced.\n",
    "\n",
    "## Implementing Deutsch's Algorithm\n",
    "\n",
    "We implement the circuit solving Deutsch's problem for one of the four cases, $f(x) = x$, with the Q# code below. First we produce an oracle $U_f$. This is clearly just the `CNOT` gate here (so we needn't define a new operation, but we do so to provide a starting point for the next exercise)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"Oracle_f1\",\"Oracle_f2\",\"Oracle_f3\",\"Oracle_f4\"]",
      "text/html": [
       "<ul><li>Oracle_f1</li><li>Oracle_f2</li><li>Oracle_f3</li><li>Oracle_f4</li></ul>"
      ],
      "text/plain": [
       "Oracle_f1, Oracle_f2, Oracle_f3, Oracle_f4"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "//oracle for identity map\n",
    "operation Oracle_f1 (x : Qubit, y : Qubit) : Unit {\n",
    "    CNOT(x,y);\n",
    "}\n",
    "//oracle for zero function\n",
    "operation Oracle_f2 (x : Qubit, y : Qubit) : Unit {\n",
    "}\n",
    "\n",
    "//oracle for f(x)=1-x\n",
    "operation Oracle_f3 (x : Qubit, y : Qubit) : Unit {\n",
    "    X(x);\n",
    "    CNOT(x,y);\n",
    "    X(x);\n",
    "}\n",
    "\n",
    "//oracle for one function\n",
    "operation Oracle_f4(x : Qubit, y : Qubit) : Unit {\n",
    "    X(y);\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we define the circuit described above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"Deutsch_solve\"]",
      "text/html": [
       "<ul><li>Deutsch_solve</li></ul>"
      ],
      "text/plain": [
       "Deutsch_solve"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "operation Deutsch_solve(): Bool {\n",
    "    \n",
    "    using(qubits=Qubit[2]){\n",
    "        H(qubits[0]); // place first qubit in |+> state\n",
    "        X(qubits[1]);\n",
    "        H(qubits[1]); // place second qubit in |-> state\n",
    "        Oracle_f1(qubits[0],qubits[1]);  // apply oracle\n",
    "        H(qubits[0]);  //\n",
    "        let val= M(qubits[0])==One;\n",
    "        ResetAll(qubits); // Release qubits\n",
    "        return val;\n",
    "\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since $f(x) = x$ is a balanced function, the final result of measuring the qubit in the computational basis should be $|1\\rangle$, hence the boolean value be `true`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "true",
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%simulate Deutsch_solve"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise:** Write oracles for the other three functions $f: \\{0,1\\} \\rightarrow \\{0,1\\}$ and verify that the problem is solved correctly in each case."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Deutsch-Jozsa algorithm for arbitrary $n$\n",
    "\n",
    "The insight gained from the $n=1$ case extends fairly straightforwardly. Suppose we have a black box function $f: \\{0,1\\}^n \\rightarrow \\{0,1\\}$, guaranteed to be either balanced or constant. We implement a quantum circuit involving a unitary oracle $U_f$ as before, measurements of which discriminate between constant and balanced functions.\n",
    "\n",
    "We begin with a quantum register of length $n+1$, with each qubit initialized as usual in the zero state i.e. the full state of the register is the tensor product state $  |0 \\rangle ^{\\otimes n+1}$. \n",
    "\n",
    "The first step is to invert the final qubit so that the register has state $  |\\psi_0 \\rangle = |0 \\rangle ^{\\otimes n} |1\\rangle $.\n",
    "\n",
    "Now apply a Hadamard gate to each qubit, so that the first $n$ qubits end up in a uniform superposition of all $2^n$ possible states, and the final qubit is in the superposition $|-\\rangle = \\frac{1}{\\sqrt{2}}\\left( |0\\rangle - |1\\rangle \\right)$, leaving the register in the state\n",
    "\n",
    "$$ |\\psi_1 \\rangle = \\sum_{x \\in \\{0,1\\}^n}  \\frac{|x\\rangle}{\\sqrt{2}^n}\\frac{1}{\\sqrt{2}}\\left( |0\\rangle - |1\\rangle \\right). $$\n",
    "\n",
    "We now define the oracle $U_f$ by analogy with the $n=1$ case by $ U_f |x\\rangle = (-1)^{f(x)} |x\\rangle$. Applying the oracle gives\n",
    "\n",
    "$$ |\\psi_2 \\rangle = U_f |\\psi_1 \\rangle =  \\sum_{x \\in \\{0,1\\}^n } \\frac{(-1)^{f(x)}}{\\sqrt{2}^n} |x\\rangle \\frac{1}{\\sqrt{2}}\\left( |0\\rangle - |1\\rangle \\right).   $$\n",
    "\n",
    "Applying a Hadamard gate to the first $n$ registers now supplies the answer, but this is hard to see immediately, so we approach the calculation in stages.\n",
    "\n",
    "Consider a single qubit $| x\\rangle$.  For the two cases $x=0$ and $x=1$ we get\n",
    "\n",
    "$$ H|0\\rangle = \\frac{1}{\\sqrt{2}}\\left( |0\\rangle + |1\\rangle\\right), \\qquad H|1\\rangle = \\frac{1}{\\sqrt{2}}\\left( |0\\rangle - |1\\rangle\\right), $$ \n",
    "\n",
    "which we can write more concisely as\n",
    "\n",
    "$$ H|x\\rangle = \\frac{1}{\\sqrt{2}}  \\sum_{z \\in\\{0,1\\}} (-1)^{xz} |z\\rangle.$$ \n",
    "\n",
    "By extension, applying the Hadamard gate to an $n$ qubit state $|x_1, \\ldots x_n \\rangle$ gives\n",
    "\n",
    "$$ H^{\\otimes}|x_1, \\ldots x_n \\rangle = \\sum_{z \\in\\{0,1\\}^n}  \\frac{(-1)^{x_1z_1 + x_2z_2 + \\ldots + x_nz_n}}{\\sqrt{2}^n} |z\\rangle =  \\sum_{z \\in\\{0,1\\}^n}  \\frac{(-1)^{x \\cdot z}}{\\sqrt{2}^n} |z\\rangle, $$\n",
    "\n",
    "using $x\\cdot z = \\sum_{i=1}^n x_i z_i$ to denote the bit-wise product (inner product). These intermediate steps allow us to compute the effect of applying the Hadamard gate to the first $n$ registers.\n",
    "\n",
    "$$ |\\psi_3 \\rangle = H^{\\otimes n} \\otimes I |\\psi_2 \\rangle = \\sum_{z \\in\\{0,1\\}^n} \\sum_{x \\in\\{0,1\\}^n} \\frac{(-1)^{x \\cdot z + f(x)}}{2^n} |z\\rangle \\frac{1}{\\sqrt{2}}\\left( |0\\rangle - |1\\rangle\\right).$$\n",
    "\n",
    "We now measure the first $n$ qubits in the computational basis. This allows us to determine whether our black box function $f$ is constant or balanced, as follows. Consider the amplitude of the state $|0\\rangle^\\otimes$. This is clearly \n",
    "\n",
    "$$\\sum_{x \\in \\{0,1\\}^n } \\frac{(-1)^{f(x)}}{2^n}$$.\n",
    "\n",
    "If $f$ is a constant function, then this just evaluates to $\\pm 1$, with the sign depending on the constant value taken. Since this amplitude has absolute value $1$, it follows that all other states must have amplitude zero, so for a constant function our measurement will yield $|0\\rangle^\\otimes$. However, for a balanced function, positive and negative contributions cancel, leaving an amplitude of zero to measure this state. Hence if $f$ is balanced, the measurement will yield at least one entry in the register with a non-zero value. This shows that we can discriminate effectively between the two classes of function.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Implementing the Deutsch-Jozsa algorithm\n",
    "\n",
    "The tasks below in cells containing the `%kata` command are adapted from the Microsoft Quantum Katas in Q#. When the cell is run, unit tests are performed to check the correctness of the code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To begin, first prepare this notebook for execution (if you skip the first step, you'll get \"Syntax does not match any known patterns\" error when you try to execute Q# code in the next cells; if you skip the second step, you'll get \"Invalid kata name\" error):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%package Microsoft.Quantum.Katas::0.6.1905.301"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part I. Oracles\n",
    "\n",
    "In this section you will implement oracles defined by classical functions using the following rules:\n",
    " - a function $f\\left(x_0, ..., x_{N-1}\\right)$ with N bits of input $x = \\left(x_0, ..., x_{N-1}\\right)$ and 1 bit of output $y$\n",
    "   defines an oracle which acts on N input qubits and 1 output qubit.\n",
    " - the oracle effect on qubits in computational basis states is defined as follows:\n",
    "   $|x\\rangle |y\\rangle \\to |x\\rangle |y \\oplus f(x)\\rangle$   ($\\oplus$ is addition modulo 2).\n",
    " - the oracle effect on qubits in superposition is defined following the linearity of quantum operations.\n",
    " - the oracle must act properly on qubits in all possible input states.\n",
    " \n",
    "You can read more about quantum oracles in [Q# documentation](https://docs.microsoft.com/en-us/quantum/concepts/oracles)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.1. $f(x) = 0$\n",
    "\n",
    "**Inputs:** \n",
    "1. N qubits in an arbitrary state $|x\\rangle$ (input register)\n",
    "2. a qubit in an arbitrary state $|y\\rangle$ (output qubit)\n",
    "\n",
    "\n",
    "**Goal:**  transform state $|x, y\\rangle$ into state $|x, y \\oplus f(x)\\rangle$ ($\\oplus$ is addition modulo 2)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T11_Oracle_Zero_Test \n",
    "\n",
    "operation Oracle_Zero (x : Qubit[], y : Qubit) : Unit {\n",
    "    // Since f(x) = 0 for all values of x, |y ⊕ f(x)⟩ = |y⟩.\n",
    "    // This means that the operation doesn't need to do any transformation to the inputs.\n",
    "    \n",
    "    // Run the cell (using Ctrl/⌘ + Enter) to see that the test passes.\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.2. $f(x) = 1$\n",
    "\n",
    "**Inputs:** \n",
    "1. N qubits in an arbitrary state $|x\\rangle$ (input register)\n",
    "2. a qubit in an arbitrary state $|y\\rangle$ (output qubit)\n",
    "\n",
    "\n",
    "**Goal:**  transform state $|x, y\\rangle$ into state $|x, y \\oplus f(x)\\rangle$ ($\\oplus$ is addition modulo 2).\n",
    "\n",
    "<br/>\n",
    "<details>\n",
    "  <summary>Need a hint? Click here</summary>\n",
    "  Since $f(x) = 1$ for all values of x, $|y \\oplus f(x)\\rangle = |y \\oplus 1\\rangle = |NOT y\\rangle$.\n",
    "  This means that the operation needs to flip qubit y (i.e. transform $|0\\rangle$ to $|1\\rangle$ and vice versa).\n",
    "</details>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T12_Oracle_One_Test \n",
    "\n",
    "operation Oracle_One (x : Qubit[], y : Qubit) : Unit {\n",
    "    // ...\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.3. $f(x) = x_k$ (the value of k-th qubit)\n",
    "\n",
    "**Inputs:** \n",
    "1. N qubits in an arbitrary state $|x\\rangle$ (input register)\n",
    "2. a qubit in an arbitrary state $|y\\rangle$ (output qubit)\n",
    "3. 0-based index of the qubit from input register ($0 \\le k < N$)\n",
    "\n",
    "**Goal:**  transform state $|x, y\\rangle$ into state $|x, y \\oplus x_k\\rangle$ ($\\oplus$ is addition modulo 2)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T13_Oracle_Kth_Qubit_Test \n",
    "\n",
    "open Microsoft.Quantum.Diagnostics;\n",
    "\n",
    "operation Oracle_Kth_Qubit (x : Qubit[], y : Qubit, k : Int) : Unit {\n",
    "    // The following line enforces the constraints on the value of k that you are given.\n",
    "    // You don't need to modify it. Feel free to remove it, this won't cause your code to fail.\n",
    "    EqualityFactB(0 <= k and k < Length(x), true, \"k should be between 0 and N-1, inclusive\");\n",
    "\n",
    "    // ...\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.4. f(x) = 1 if x has odd number of 1s, and 0 otherwise\n",
    "\n",
    "**Inputs:** \n",
    "1. N qubits in an arbitrary state $|x\\rangle$ (input register)\n",
    "2. a qubit in an arbitrary state $|y\\rangle$ (output qubit)\n",
    "\n",
    "\n",
    "**Goal:**  transform state $|x, y\\rangle$ into state $|x, y \\oplus f(x)\\rangle$ ($\\oplus$ is addition modulo 2).\n",
    "\n",
    "<br/>\n",
    "<details>\n",
    "  <summary>Need a hint? Click here</summary>\n",
    "  $f(x)$ can be represented as $x_0 \\oplus x_1 \\oplus ... \\oplus x_{N-1}$.\n",
    "</details>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T14_Oracle_OddNumberOfOnes_Test\n",
    "\n",
    "operation Oracle_OddNumberOfOnes (x : Qubit[], y : Qubit) : Unit {\n",
    "    // ...\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.5. $f(x) = \\bigoplus\\limits_{i=0}^{N-1} r_i x_i$ for a given bit vector r (scalar product function)\n",
    "\n",
    "**Inputs:** \n",
    "1. N qubits in an arbitrary state $|x\\rangle$ (input register)\n",
    "2. a qubit in an arbitrary state $|y\\rangle$ (output qubit)\n",
    "3. a bit vector of length N represented as an `Int[]`.\n",
    "   You are guaranteed that the qubit array and the bit vector have the same length.\n",
    "\n",
    "**Goal:**  transform state $|x, y\\rangle$ into state $|x, y \\oplus f(x)\\rangle$ ($\\oplus$ is addition modulo 2)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T15_Oracle_ProductFunction_Test\n",
    "\n",
    "open Microsoft.Quantum.Diagnostics;\n",
    "\n",
    "operation Oracle_ProductFunction (x : Qubit[], y : Qubit, r : Int[]) : Unit {\n",
    "    // The following line enforces the constraint on the input arrays.\n",
    "    // You don't need to modify it. Feel free to remove it, this won't cause your code to fail.\n",
    "    EqualityFactI(Length(x), Length(r), \"Arrays should have the same length\");\n",
    "\n",
    "    // ...\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.6. $f(x) = \\bigoplus\\limits_{i=0}^{N-1} \\left(r_i x_i + (1 - r_i) (1 - x_i) \\right)$ for a given bit vector r (scalar product function)\n",
    "\n",
    "**Inputs:** \n",
    "1. N qubits in an arbitrary state $|x\\rangle$ (input register)\n",
    "2. a qubit in an arbitrary state $|y\\rangle$ (output qubit)\n",
    "3. a bit vector of length N represented as an `Int[]`.\n",
    "   You are guaranteed that the qubit array and the bit vector have the same length.\n",
    "\n",
    "**Goal:**  transform state $|x, y\\rangle$ into state $|x, y \\oplus f(x)\\rangle$ ($\\oplus$ is addition modulo 2).\n",
    "\n",
    "<br/>\n",
    "<details>\n",
    "  <summary>Need a hint? Click here</summary>\n",
    "  Since each addition is done modulo 2, you can evaluate the effect of each term independently$.\n",
    "</details>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T16_Oracle_ProductWithNegationFunction_Test\n",
    "\n",
    "open Microsoft.Quantum.Diagnostics;\n",
    "\n",
    "operation Oracle_ProductWithNegationFunction (x : Qubit[], y : Qubit, r : Int[]) : Unit {\n",
    "    // The following line enforces the constraint on the input arrays.\n",
    "    // You don't need to modify it. Feel free to remove it, this won't cause your code to fail.\n",
    "    EqualityFactI(Length(x), Length(r), \"Arrays should have the same length\");\n",
    "\n",
    "    // ...\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.7. $f(x) = \\bigoplus\\limits_{i=0}^{N-1} x_i + $ (1 if prefix of x is equal to the given bit vector, and 0 otherwise) modulo 2\n",
    "\n",
    "**Inputs:** \n",
    "1. N qubits in an arbitrary state $|x\\rangle$ (input register)\n",
    "2. a qubit in an arbitrary state $|y\\rangle$ (output qubit)\n",
    "3. a bit vector of length $K$ represented as an `Int[]` ($1 \\le K \\le N$).\n",
    "\n",
    "**Goal:**  transform state $|x, y\\rangle$ into state $|x, y \\oplus f(x)\\rangle$ ($\\oplus$ is addition modulo 2).\n",
    "\n",
    "> A prefix of length K of a state $|x\\rangle = |x_0, ..., x_{N-1}\\rangle$ is the state of its first K qubits $|x_0, ..., x_{K-1}\\rangle$. For example, a prefix of length 2 of a state $|0110\\rangle$ is 01.\n",
    "\n",
    "<br/>\n",
    "<details>\n",
    "  <summary>Need a hint? Click here</summary>\n",
    "  The first term is the same as in task 1.4. To implement the second term, you can use `Controlled` functor which allows to perform multicontrolled gates (gates with multiple control qubits).\n",
    "</details>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T17_Oracle_HammingWithPrefix_Test\n",
    "\n",
    "open Microsoft.Quantum.Diagnostics;\n",
    "\n",
    "operation Oracle_HammingWithPrefix (x : Qubit[], y : Qubit, prefix : Int[]) : Unit {\n",
    "    // The following line enforces the constraint on the input arrays.\n",
    "    // You don't need to modify it. Feel free to remove it, this won't cause your code to fail.\n",
    "    let K = Length(prefix);\n",
    "    EqualityFactB(1 <= K and K <= Length(x), true, \"K should be between 1 and N, inclusive\");\n",
    "\n",
    "    // ...\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.8. f(x) = 1 if x has two or three bits (out of three) set to 1, and 0 otherwise (majority function)\n",
    "\n",
    "**Inputs:** \n",
    "1. 3 qubits in an arbitrary state $|x\\rangle$ (input register)\n",
    "2. a qubit in an arbitrary state $|y\\rangle$ (output qubit)\n",
    "\n",
    "\n",
    "**Goal:**  transform state $|x, y\\rangle$ into state $|x, y \\oplus f(x)\\rangle$ ($\\oplus$ is addition modulo 2).\n",
    "\n",
    "<br/>\n",
    "<details>\n",
    "  <summary>Need a hint? Click here</summary>\n",
    "  Represent f(x) in terms of AND and $\\oplus$ operations.\n",
    "</details>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T18_Oracle_MajorityFunction_Test\n",
    "\n",
    "open Microsoft.Quantum.Diagnostics;\n",
    "\n",
    "operation Oracle_MajorityFunction (x : Qubit[], y : Qubit) : Unit {\n",
    "    // The following line enforces the constraint on the input array.\n",
    "    // You don't need to modify it. Feel free to remove it, this won't cause your code to fail.\n",
    "    EqualityFactI(3, Length(x), \"x should have exactly 3 qubits\");\n",
    "\n",
    "    // ...\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part II. Deutsch-Jozsa Algorithm\n",
    "\n",
    "In this section you will implement the Deutsch-Jozsa algorithm and run it on the oracles you've defined in part I to observe the results. \n",
    "\n",
    "This algorithm solves the following problem. You are given a quantum oracle which implements a classical function $f(x): \\{0, 1\\}^N \\to \\{0, 1\\}$. You are guaranteed that the function $f$ is either constant (has the same value for all inputs) or balanced (has value 0 for half of the inputs and 1 for the other half of the inputs). The goal of the algorithm is to figure out whether the function is constant or balanced in just one oracle call.\n",
    " \n",
    "You can read more about the Deutsch-Jozsa algorithm in [Wikipedia](https://en.wikipedia.org/wiki/Deutsch%E2%80%93Jozsa_algorithm)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 2.1. Deutsch-Jozsa Algorithm\n",
    "\n",
    "**Inputs:** \n",
    "1. the number of qubits $N$ in the input register for the function f\n",
    "2. a quantum operation which implements the oracle $|x, y\\rangle \\to |x, y \\oplus f(x)\\rangle$, where x is an $N$-qubit input register, y is a 1-qubit answer register, and f is a Boolean function\n",
    "\n",
    "\n",
    "**Output:**  `true` if the function f is constant, or `false` if the function f is balanced."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T31_DJ_Algorithm_Test\n",
    "\n",
    "operation DJ_Algorithm (N : Int, oracle : ((Qubit[], Qubit) => Unit)) : Bool {\n",
    "    // Create a boolean variable for storing the return value.\n",
    "    // You'll need to update it later, so it has to be declared as mutable.\n",
    "    // ...\n",
    "\n",
    "    // Allocate an array of N qubits for the input register x and one qubit for the answer register y.\n",
    "    using ((x, y) = (Qubit[N], Qubit())) {\n",
    "        // Newly allocated qubits start in the |0⟩ state.\n",
    "        // The first step is to prepare the qubits in the required state before calling the oracle.\n",
    "        // Each qubit of the input register has to be in the |+⟩ state.\n",
    "        // ...\n",
    "\n",
    "        // The answer register has to be in the |-⟩ state.\n",
    "        // ...\n",
    "\n",
    "        // Apply the oracle to the input register and the answer register.\n",
    "        // ...\n",
    "\n",
    "        // Apply a Hadamard gate to each qubit of the input register again.\n",
    "        // ...\n",
    "\n",
    "        // Measure each qubit of the input register in the computational basis using the M operation.\n",
    "        // If any of the measurement results is One, the function implemented by the oracle is balanced.\n",
    "        // ...\n",
    "\n",
    "        // Before releasing the qubits make sure they are all in the |0⟩ state.\n",
    "        // ...\n",
    "    }\n",
    "    \n",
    "    // Return the answer.\n",
    "    // ...\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 2.2. Running Deutsch-Jozsa Algorithm\n",
    "\n",
    "**Goal**: Use your implementation of Deutsch-Jozsa algorithm from task 2.1 to test each of the oracles you've implemented in part I for being constant or balanced.\n",
    "\n",
    "> This is an open-ended task, and is not covered by a unit test. To run the code, execute the cell with the definition of the `Run_DeutschJozsa_Algorithm` operation first; if it compiled successfully without any errors, you can run the operation by executing the next cell (`%simulate Run_DeutschJozsa_Algorithm`).\n",
    "\n",
    "> Note that this task relies on your implementations of the previous tasks. If you are getting the \"No variable with that name exists.\" error, you might have to execute previous code cells before retrying this task."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "open Microsoft.Quantum.Diagnostics;\n",
    "\n",
    "operation Run_DeutschJozsa_Algorithm () : String {\n",
    "    // You can use EqualityFactB function to represent the invariant that the return value of DJ_Algorithm operation matches the expected value\n",
    "    EqualityFactB(DJ_Algorithm(4, Oracle_Zero), true, \"f(x) = 0 not identified as constant\");\n",
    "    \n",
    "    // Run the algorithm for the rest of the oracles\n",
    "    // ...\n",
    "    \n",
    "    // If all tests pass, report success!\n",
    "    return \"Success!\";\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%simulate Run_DeutschJozsa_Algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part III. Bernstein–Vazirani Algorithm\n",
    "\n",
    "In this section you will implement the Bernstein-Vazirani algorithm and run it on the oracles you've defined in part I to observe the results. \n",
    "\n",
    "This algorithm solves the following problem. You are given a quantum oracle which implements a classical function $f(x): \\{0, 1\\}^N \\to \\{0, 1\\}$. You are guaranteed that the function $f$ can be represented as a scalar product, i.e., there exists a bit vector $r = (r_0, ..., r_{N-1})$ such that $f(x) = \\bigoplus \\limits_{i=0}^{N-1} x_i r_i$. The goal of the algorithm is to reconstruct the bit vector $r$ in just one oracle call.\n",
    " \n",
    "You can read more about the Bernstein-Vazirani algorithm in [arXiv:1804.03719](https://arxiv.org/abs/1804.03719)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# (Optional) \n",
    "\n",
    "### Task 3.1. Bernstein-Vazirani Algorithm\n",
    "\n",
    "**Inputs:** \n",
    "1. the number of qubits $N$ in the input register for the function f\n",
    "2. a quantum operation which implements the oracle $|x, y\\rangle \\to |x, y \\oplus f(x)\\rangle$, where x is an $N$-qubit input register, y is a 1-qubit answer register, and f is a Boolean function\n",
    "\n",
    "\n",
    "**Output:**  The bit vector $r$ reconstructed from the oracle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%kata T22_BV_Algorithm_Test\n",
    "\n",
    "operation BV_Algorithm (N : Int, oracle : ((Qubit[], Qubit) => Unit)) : Int[] {\n",
    "    // The algorithm is very similar to Deutsch-Jozsa algorithm; try to implement it without hints.\n",
    "    // ...\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 3.2. Running Bernstein-Vazirani Algorithm\n",
    "\n",
    "**Goal**: Use your implementation of Bernstein-Vazirani algorithm from task 3.1 to reconstruct the hidden vector $r$ for the oracles you've implemented in part I.\n",
    "\n",
    "> This is an open-ended task, and is not covered by a unit test. To run the code, execute the cell with the definition of the `Run_BernsteinVazirani_Algorithm` operation first; if it compiled successfully without any errors, you can run the operation by executing the next cell (`%simulate Run_BernsteinVazirani_Algorithm`).\n",
    "\n",
    "> Note that this task relies on your implementations of the previous tasks. If you are getting the \"No variable with that name exists.\" error, you might have to execute previous code cells before retrying this task.\n",
    "\n",
    "<details>\n",
    "  <summary>Need a hint? Click here</summary>\n",
    "  Not all oracles from part I can be represented as scalar product functions. The most generic oracle you can use in this task is Oracle_ProductFunction from task 1.5; Oracle_Zero, Oracle_Kth_Qubit and Oracle_OddNumberOfOnes are special cases of this oracle.\n",
    "</details>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Start by implementing a function AllEqualityFactI \n",
    "// to check the results of applying the algorithm to each oracle in a uniform manner.\n",
    "function AllEqualityFactI(actual : Int[], expected : Int[]) : Bool {\n",
    "    // Check that array lengths are equal\n",
    "    // ...\n",
    "    \n",
    "    // Check that the corresponding elements of the arrays are equal\n",
    "    // ...\n",
    "    fail \"AllEqualityFactI is not implemented\";\n",
    "}\n",
    "\n",
    "operation Run_BernsteinVazirani_Algorithm () : String {\n",
    "    // Now use AllEqualityFactI to verify the results of the algorithm\n",
    "    if (not AllEqualityFactI(BV_Algorithm(3, Oracle_Zero), [0, 0, 0])) {\n",
    "        return \"Incorrect result for f(x) = 0\";\n",
    "    }\n",
    "    \n",
    "    // Run the algorithm on the rest of the oracles\n",
    "    // ...\n",
    "    \n",
    "    // If all tests pass, report success!\n",
    "    return \"Success!\";\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%simulate Run_BernsteinVazirani_Algorithm"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Q#",
   "language": "qsharp",
   "name": "iqsharp"
  },
  "language_info": {
   "file_extension": ".qs",
   "mimetype": "text/x-qsharp",
   "name": "qsharp",
   "version": "0.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
