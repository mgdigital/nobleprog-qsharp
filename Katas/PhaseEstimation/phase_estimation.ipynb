{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Phase Estimation and the Quantum Fourier Transform\n",
    "\n",
    "One idea above all has caused quantum computing to catch the public imagination. In 1994, Peter Shor proposed an algorithm that would allow a quantum computer to carry out efficient factorization of a number into its prime factors. Factorization of large integers is believed to be a difficult problem, so much so that its difficulty underpins the security of the most widely used methods for encryption. A sufficiently powerful quantum computer executing Shor's algorithm would be able to undermine modern cryptographic methods, and so would have serious implications for our ability to communicate securely.\n",
    "\n",
    "Shor's algorithm is an application of the method of **phase estimation**, which provides approximations to the **eigenvalues** of unitary operators. In turn, phase estimation makes use of the **quantum Fourier transform**, an algorithm for efficiently determining the Fourier transform of quantum mechanical amplitudes.\n",
    "    \n",
    "## The quantum Fourier transform\n",
    "\n",
    "The Fourier transform is very widely used throughout the physical sciences. It is a linear map, which sends an input vector $(x_0, \\ldots, x_{N-1})$ to the vector with entries\n",
    "\n",
    "$$ y_k = \\frac{1}{\\sqrt{N}} \\sum_{j=0}^{N-1} x_j e^{2\\pi i j k/N}. $$\n",
    "\n",
    "By analogy, we define the **quantum Fourier transform** on a collection $|0\\rangle \\ldots |N-1\\rangle$ of basis states as the unitary map that takes the state $|j\\rangle$ to \n",
    "\n",
    "$$ U|j \\rangle = \\frac{1}{\\sqrt{N}} \\sum_{k=0}^{N-1} e^{2\\pi i j k/N} |k \\rangle.  $$\n",
    "\n",
    "\n",
    "Note what happens here - the QFT takes the total amplitude of the state $|j\\rangle$ and distributes it amongst all of the basis states. We might even say that it distributes the amplitude *equally*, since the amplitude assigned to a state $|k\\rangle$ is $\\frac{e^{2\\pi i j k/N}}{\\sqrt{N}}$, which has absolute value $1/\\sqrt{N}$, independent of $j$ and $k$. Moreover, it does so in a *different* way for each $|j\\rangle$ - an orthonormal basis is mapped to an orthonormal basis.\n",
    "\n",
    "\n",
    "## An example\n",
    "\n",
    "The quantum Fourier transform is a linear map, so it can be written as a matrix with respect to the computational basis. Working with N=4, we can easily write down the matrix that represents the QFT. Up to a factor of $\\sqrt{N}$, the matrix is\n",
    "\n",
    "$$\n",
    "U_4 = \n",
    "\\begin{pmatrix}\n",
    "1 & 1 & 1 & 1 \\\\\n",
    "1 & i & i^2 & i^3 \\\\\n",
    "1 & i^2 & i^4 & i^6 \\\\\n",
    "1 & i^3 & i^6 & i^9 \n",
    "\\end{pmatrix} \n",
    "=\n",
    "\\begin{pmatrix}\n",
    "1 & 1 & 1 & 1 \\\\\n",
    "1 & i & -1 & -i \\\\\n",
    "1 & -1 & 1 & -1 \\\\\n",
    "1 & -i & -1 & i \n",
    "\\end{pmatrix} \n",
    "$$ \n",
    "\n",
    "It is straightforward to see that $U_4$ is a unitary operator, by multiplying by its conjugate. Equivalently, it is clear that the matrix has orthonormal columns. \n",
    "\n",
    "\n",
    "**Question** With $n$ qubits, what state results from applying the QFT to the basis state $|00\\ldots0\\rangle$?\n",
    "\n",
    "\n",
    "\n",
    "## A circuit for the quantum Fourier transform\n",
    "\n",
    "\n",
    "For the cases we will consider, it is best to work with $N=2^n$ for some $n$. So then we can take the basis $|0\\rangle \\ldots |N-1\\rangle$ to be the computational basis for a quantum register on $n$ qubits. We can write the number $j$ in binary notation, so that \n",
    "\n",
    "$$j = \\sum_{k=1}^n j_{k}2^{n-k}, \\qquad j_k \\in \\{0,1\\},  $$\n",
    "\n",
    "or equivalently, we can write a binary expansion \n",
    "\n",
    "$$0.j_1 \\ldots j_n  =   \\sum_{k=1}^n \\frac{j_k}{2^k}.$$\n",
    "\n",
    "This representation gives a way of writing the QFT that is as elegant as could ever be hoped for.\n",
    "\n",
    "$$ U| j_1, \\ldots j_n \\rangle = \\frac{1}{\\sqrt{2}^N} \\left( |0\\rangle + e^{2\\pi i 0.j_n}|1\\rangle  \\right) \\left(|0\\rangle + e^{2\\pi i 0.j_{n-1}j_n}|1\\rangle\n",
    "   \\right) \\ldots \\left( |0\\rangle + e^{2\\pi i 0.j_1j_2 \\ldots j_{n-1}j_n}|1\\rangle  \\right). $$\n",
    "   \n",
    " \n",
    "One reason that this representation is so useful is that it makes clear how to implement the QFT in terms of primitive gates. The effect on the $k$th bit of the register can be obtained by applying a controlled rotation.\n",
    "\n",
    "$$\n",
    "R_k = \\begin{pmatrix}\n",
    "1 & 0 \\\\\n",
    "0 & e^{\\frac{2\\pi i}{2^k}} \n",
    "\\end{pmatrix}.\n",
    "$$ \n",
    "\n",
    "The QFT will be a key ingredient in the **phase estimation algorithm**, which we now discuss. The exercises below are adapted from the Microsoft Quantum Katas. Unit tests will help you to check your code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"Microsoft.Quantum.Standard::0.6.1905.301\",\"Microsoft.Quantum.Katas::0.6.1905.301\"]",
      "text/html": [
       "<ul><li>Microsoft.Quantum.Standard::0.6.1905.301</li><li>Microsoft.Quantum.Katas::0.6.1905.301</li></ul>"
      ],
      "text/plain": [
       "Microsoft.Quantum.Standard::0.6.1905.301, Microsoft.Quantum.Katas::0.6.1905.301"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%package Microsoft.Quantum.Katas::0.6.1905.301"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ingredients of Phase Estimation\n",
    "    \n",
    "In order to apply quantum phase estimation to an operator $U$, we must first prepare a register in an eigenstate $|u\\rangle$. The input to your first task is a qubit in the $|0\\rangle$ state, and your task is to prepare it in an eigenstate of the $Z$ gate corresponding to the eigenvalue specified in `state`, which can be $0$ or $1$.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "\"Success!\"",
      "text/plain": [
       "Success!"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%kata T11_Eigenstates_ZST_Test\n",
    "operation Eigenstates_ZST (q : Qubit, state : Int) : Unit\n",
    "    is Adj {\n",
    "        if(state ==1){\n",
    "            X(q);\n",
    "        }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For phase estimation, we need to be able to apply powers of the unitary operator $U$, as controlled gates.\n",
    "\n",
    "For this task, first complete the operation `UnitaryPowerImpl`, which takes as its arguments a unitary operator $U$, a positive whole number `power` and a qubit `q`.\n",
    "\n",
    "Then, complete the wrapper function, which takes as its arguments a unitary operator $U$ and a whole number `power` and returns an operation $U^{power}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"UnitaryPowerImpl\"]",
      "text/html": [
       "<ul><li>UnitaryPowerImpl</li></ul>"
      ],
      "text/plain": [
       "UnitaryPowerImpl"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "operation UnitaryPowerImpl (U : (Qubit => Unit is Adj + Ctl), power : Int, q : Qubit) : Unit\n",
    "    is Adj + Ctl {\n",
    "        for (i in 1..power) {\n",
    "            U(q);\n",
    "        }\n",
    "    }\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "\"Success!\"",
      "text/plain": [
       "Success!"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%kata T12_UnitaryPower_Test\n",
    "\n",
    "   function UnitaryPower (U : (Qubit => Unit is Adj + Ctl), power : Int) : (Qubit => Unit is Adj + Ctl) {\n",
    "        return UnitaryPowerImpl(U,power,_);\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Phase Estimation Algorithm\n",
    "\n",
    "With our preliminary work out of the way, the phase estimation algorithm proceeds cleanly.\n",
    "\n",
    "We use the matrix $P$ (given) to prepare one qubit in an eigenstate $|\\psi\\rangle$ of $U$, with eigenvalue $e^{2\\pi i \\theta}$. Note that as a unitary matrix, $U$ can only have eigenvalues of modulus $1$, i.e. those of the form given, for some $0 \\le \\theta < 2 \\pi$.\n",
    "\n",
    "Then we prepare a register of $n$ target qubits, in which to store the estimated value. The value of $n$ chosen reflects the precision of the estimate that is required - the register will store an estimate of the most significant $n$ bits of the phase. To fix ideas, suppose that $\\theta$ can be written as a binary expansion $0.\\theta_1\\theta_2 \\ldots \\theta_n$, for $\\theta_k \\in \\{0,1\\}$. If not, what is obtained below is an $n$-bit approximation to $\\theta$.\n",
    "\n",
    "The target register is placed in the maximally superposed state, by applying the Hadamard gate.\n",
    "\n",
    "Now the rightmost qubit is in the state $|+\\rangle = \\frac{1}{\\sqrt{2}} \\left( |0\\rangle + |1\\rangle  \\right)$. To this qubit, we apply $U$, controlled on the final qubit, to get the state\n",
    "\n",
    "$$  \\frac{1}{\\sqrt{2}} \\left( |0\\rangle + e^{2\\pi i \\theta}|1\\rangle  \\right).$$\n",
    "\n",
    "Moving further along the register to the $j$th entry, we apply the gate $U^{2^j}$, again controlled on the final qubit, so that its state becomes\n",
    "\n",
    "$$  \\frac{1}{\\sqrt{2}} \\left( |0\\rangle + e^{2\\pi i 2^{j} \\theta}|1\\rangle  \\right).$$\n",
    "\n",
    "After this routine completes, the state of the $n$ target bits will be\n",
    "\n",
    "$$  \\frac{1}{\\sqrt{2}^n}\\left( |0\\rangle + e^{2\\pi i 2^{n-1} \\theta}|1\\rangle  \\right) \\ldots  \\left( |0\\rangle + e^{2\\pi i 2^{j} \\theta}|1\\rangle  \\right) \\ldots \\left( |0\\rangle + e^{2\\pi i \\theta}|1\\rangle  \\right),$$\n",
    "\n",
    "which can be expanded into a sum to give\n",
    "\n",
    "$$  \\frac{1}{\\sqrt{2}^{N}}\\sum_{k=0}^{2^n-1}  e^{2\\pi i \\theta k}|k\\rangle. $$\n",
    "\n",
    "It is precisely at this point that the quantum Fourier transform can be used. You may already have noticed the similarity of the product representation with that given for the QFT. The expression above is precisely the QFT of the (n bit approximation to) the phase $\\theta$. So applying the inverse of the QFT (i.e. its adjoint, because it is a unitary map) returns the phase (or an n-bit approximation to it).\n",
    "\n",
    "The algorithm is best visualized as the circuit given in the figure below. We will implement this circuit in the following section of the notebook.\n",
    "\n",
    "<img src=\"phase_est.png\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implementing Phase Estimation\n",
    "In this section of the notebook, we will put everything together and show how phase estimation is carried out. \n",
    "\n",
    "Suppose that we are given the following inputs\n",
    "\n",
    "1. a single-qubit unitary U.\n",
    "2. a single-qubit eigenstate of U |ψ⟩ represented by a unitary P such that |ψ⟩ = P|0⟩  (i.e., applying the unitary P to state |0⟩ prepares state |ψ⟩).\n",
    "3. an integer n.\n",
    "    \n",
    "We need to write an operation that implements the circuit above, outputting the phase of the eigenvalue that corresponds to the eigenstate $|\\psi\\rangle$, with $n$ bits of precision. The phase should be between 0 and 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"QPE_Oracle\"]",
      "text/html": [
       "<ul><li>QPE_Oracle</li></ul>"
      ],
      "text/plain": [
       "QPE_Oracle"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "operation QPE_Oracle (U : (Qubit => Unit is Adj + Ctl), power : Int, target : Qubit) : Unit \n",
    "    is Adj + Ctl{\n",
    "        for (i in 1 .. power) {\n",
    "            U(target);\n",
    "        }\n",
    "    }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "\"Success!\"",
      "text/plain": [
       "Success!"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%kata T14_QPE_Test\n",
    "\n",
    "open Microsoft.Quantum.Oracles;\n",
    "open Microsoft.Quantum.Characterization;\n",
    "open Microsoft.Quantum.Arithmetic;\n",
    "open Microsoft.Quantum.Convert;\n",
    "    \n",
    "// Task 1.4. QPE for single-qubit unitaries\n",
    "operation QPE (U : (Qubit => Unit is Adj + Ctl), P : (Qubit => Unit is Adj), n : Int) : Double {\n",
    "    // Construct a phase estimation oracle from the unitary\n",
    "    // Allocate qubits to hold the eigenstate of U and the phase in a big endian register \n",
    "    using ((eigenstate, phaseRegister) = (Qubit[1], Qubit[n])) {\n",
    "        P(eigenstate[0]);\n",
    "        ApplyToEach(H, phaseRegister);\n",
    "        for(i in 1..n){\n",
    "            Controlled QPE_Oracle(eigenstate,(U,2^(i-1),phaseRegister[i-1]));\n",
    "        }\n",
    "       \n",
    "         let phaseRegisterLE =LittleEndian(phaseRegister);\n",
    "         Adjoint ApplyQuantumFourierTransform(phaseRegisterLE);\n",
    "        let phase = IntAsDouble(MeasureInteger(phaseRegisterLE)) / IntAsDouble(1 <<< n);        \n",
    "    \n",
    "        ResetAll(eigenstate);\n",
    "        ResetAll(phaseRegister);\n",
    "        return phase;\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing and Evaluating the phase estimation algorithm.\n",
    "\n",
    "In what follows we will check that several well-known gates have the appropriate phase.\n",
    "\n",
    "The following two cells contain code that runs the phase estimation algorithm for the `X` gate using the matrix $P$ to prepare the eigenstate as the identity matrix `I`. The eigenvalue is evaluated two $2$ bits of precision. This means that the eigenstate prepared will be $|0\\rangle$, which has eigenvalue $1 = e^{2 \\pi i 0}$, so the algorithm should return a phase of $0$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"MyQPE_Test\"]",
      "text/html": [
       "<ul><li>MyQPE_Test</li></ul>"
      ],
      "text/plain": [
       "MyQPE_Test"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "operation MyQPE_Test () : Double {\n",
    "    return QPE(X,I,2);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "0.0",
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%simulate MyQPE_Test"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise:** For each of the following gates and prepared eigenstates, verify that the phase is correctly estimated. Be sure to use enough bits of precision (what happens if you do not?)\n",
    "\n",
    "1. The `Z` gate prepared using the matrix `X`.\n",
    "2. The `S` gate prepared using the matrix `X`\n",
    "3. The `T` gate prepared using the matrix `I`\n",
    "4. The `T` gate prepared using the matrix `X`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Q#",
   "language": "qsharp",
   "name": "iqsharp"
  },
  "language_info": {
   "file_extension": ".qs",
   "mimetype": "text/x-qsharp",
   "name": "qsharp",
   "version": "0.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
