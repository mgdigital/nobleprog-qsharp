{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Grover's algorithm\n",
    "\n",
    "Grover's algorithm shows how a **quantum oracle** can in principle be used to search an **unstructured database** more efficiently than is possible with classical algorithms.\n",
    "\n",
    "The set up is as follows. Suppose we wish to search a list of length $N$, to determine which index contains a particular element. We can think about this situation abstractly using a framework similar to that considered for Deutsch's algorithm. Essentially, we have a binary function $f: \\{1, \\ldots N\\} \\rightarrow \\{0,1\\}$, such that $f(k) = 1$ and $f(i) = 0$ for $i \\neq k$, where $k$ is the index of our list that contains the sought element.\n",
    "\n",
    "**Question:** thinking classically, how many steps are needed to solve this problem (a) on average, (b) in the worst case?\n",
    "\n",
    "## The oracle\n",
    "\n",
    "We begin by assuming access to an **oracle** - a black box function that can **recognize** solutions when they are presented to it. We will discuss the internal workings of such a black box later. One might initially wonder why access to an oracle does not simply solve the problem on its own. But we draw an important distinction between **finding** the solution to a problem, and **verifying** that a proposed solution is indeed correct. We are familiar with problems in which recognising solutions is easier than finding them. For instance, factorising a large number into its constitutent primes is (as far as we know) a difficult task, but it is trivial to check the value of the product of a list of proposed primes. \n",
    "\n",
    "We begin by specifying the action of the oracle as a **unitary operator**. Similarly to the oracle in the Deutsch-Jozsa problem, to make the operation unitary we work with an additional oracle *qubit*.\n",
    "\n",
    "Suppose we have an $N = 2^n$, so that we can use a register of $n$ qubits to represent the problem. We will refer to the $N$ different basis states as, say, $|x\\rangle$. We assume (for now) that there is precisely one basis state for which $f(x)=1$, although the algorithm described below can be extended to cope with a more general situation.  \n",
    "\n",
    "Define the action of the oracle $O$ on the computational basis as\n",
    "\n",
    "$$ O |x\\rangle |q\\rangle = |x\\rangle |q \\oplus f(x)\\rangle. $$\n",
    "\n",
    "So then, armed with the quantum oracle it is straightforward to verify if the state $x$ is a solution to our search problem. How? We simply prepare a quantum register in the state $|x\\rangle|0\\rangle$, apply $O$ and measure the oracle qubit. If (and only if) $x$ is a solution, the oracle qubit will be in the flipped state $|1\\rangle$.\n",
    "\n",
    "Again, as in the Deutsch-Jozsa algorithm, it is useful to initialize the oracle qubit in the state $|-\\rangle = \\frac{1}{\\sqrt{2}} \\left( |0\\rangle -|1\\rangle \\right)$.\n",
    "\n",
    "Then, by linearity of $O$, the effect of applying the oracle qubit is\n",
    "\n",
    "$$ O |x\\rangle \\frac{1}{\\sqrt{2}} \\left( |0\\rangle -|1\\rangle \\right) = \\frac{1}{\\sqrt{2}} \\left(|x\\rangle |0\\oplus f(x) \\rangle - |x\\rangle |1\\oplus f(x) \\rangle \\right).  $$ \n",
    "\n",
    "Checking the two cases $f(x) =0$ and $f(x)= 1$ directly (take a moment to do this!), we have\n",
    "\n",
    "$$  O |x\\rangle \\frac{1}{\\sqrt{2}} \\left( |0\\rangle -|1\\rangle \\right) = (-1)^{f(x)} |x\\rangle \\frac{1}{\\sqrt{2}} \\left( |0\\rangle -|1\\rangle \\right). $$\n",
    "\n",
    "Interestingly, in either case the oracle qubit remains unchanged. For brevity, we therefore omit it in what follows, and simply consider $O$ as mapping the state $|x\\rangle$ to $(-1)^{f(x)} |x\\rangle$.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The circuit\n",
    "\n",
    "We begin with an $n$-qubit register in the $|0 \\rangle ^{\\otimes n}$ state, which is then placed into the equal superposition state\n",
    "\n",
    "$$  |\\psi \\rangle = \\frac{1}{\\sqrt{N}} \\sum_{x=0}^{N-1} |x\\rangle,$$\n",
    "\n",
    "by applying a Hadamard gate to each qubit. (This result may well not be immediately obvious - write out what happens for e.g. $N=2$ to convince yourself.) \n",
    "\n",
    "The algorithm then applies the **Grover operator** repeatedly. This is a subroutine consisting of four steps\n",
    "\n",
    "1. Apply $O$ to the register.\n",
    "2. Apply the Hadamard transform $H^{\\otimes n}$ to the entire register.\n",
    "3. Apply a **conditional phase-shift** operation, which sends the state $|x\\rangle$ to $-|x\\rangle$ for all states *except* $|0\\rangle$,which is unchanged.\n",
    "4. Apply the Hadamard transform $H^{\\otimes n}$ again.\n",
    "\n",
    "\n",
    "## A small example\n",
    "\n",
    "Suppose we consider a problem with $N=4$. (It can be shown that in this case Grover's algorithm returns the correct entry after one iteration.) Say we have $k=3$ (starting count from one), which would be encoded in the register as the binary state $|10\\rangle$.\n",
    "\n",
    "We start off in the equally superposed state\n",
    "\n",
    "$$  |\\psi \\rangle =  \\frac{1}{2} \\left(|00\\rangle + |01\\rangle + |10\\rangle + |11\\rangle \\right). $$\n",
    "\n",
    "We then apply the oracle $O$, which shifts the phase of the solution basis state:\n",
    "\n",
    "$$  O |\\psi \\rangle =  \\frac{1}{2} \\left(|00\\rangle + |01\\rangle - |10\\rangle + |11\\rangle \\right). $$\n",
    "\n",
    "Next, we perform another Hadamard transformation on each qubit. For completeness, we determine the effect on each of the basis vectors.\n",
    "\n",
    "$$ H   |00\\rangle = \\frac{1}{2} \\left(|00\\rangle + |01\\rangle + |10\\rangle + |11\\rangle \\right) $$\n",
    "\n",
    "\n",
    "$$ H   |01\\rangle = \\frac{1}{2} \\left(|00\\rangle - |01\\rangle + |10\\rangle - |11\\rangle \\right) $$\n",
    "\n",
    "$$ H   |10\\rangle = \\frac{1}{2} \\left(|00\\rangle + |01\\rangle - |10\\rangle - |11\\rangle \\right) $$\n",
    "\n",
    "$$ H   |11\\rangle = \\frac{1}{2} \\left(|00\\rangle - |01\\rangle - |10\\rangle + |11\\rangle \\right) $$\n",
    "\n",
    "Adding these terms together with the appropriate signs gives\n",
    "\n",
    "$$ H O |\\psi \\rangle = \\frac{1}{2} \\left(|00\\rangle - |01\\rangle + |10\\rangle + |11\\rangle \\right). $$\n",
    "\n",
    "We now perform a conditional phase flip - all states except $|0\\rangle$ undergo phase change, giving:\n",
    "\n",
    "$$  \\frac{1}{2} \\left(|00\\rangle + |01\\rangle - |10\\rangle - |11\\rangle \\right).$$\n",
    "\n",
    "The final step is to apply another Hadamard gate. Our analysis above of the four terms, together with the fact that $H$ is self-inverse, we see that the final result is\n",
    "\n",
    "$$|10\\rangle.$$\n",
    "\n",
    "This is exactly the state we hoped to find! Grover's algorithm found the solution after precisely one iteration. In general, more steps are required, and the correct state is found only with high probability.\n",
    "\n",
    "## Understanding Grover's algorithm\n",
    "\n",
    "As presented so far, Grover's algorithm is quite opaque. But in fact, it has a very elegant geometric formulation. To understand this, it will be necessary to work through some linear algebra using the bra-ket notation.\n",
    "\n",
    "First, note that the conditional phase shift operation used in step 3 can be written as\n",
    "\n",
    "$$ 2|0\\rangle\\langle 0| - I.$$\n",
    "\n",
    "This is straightforward to verify, by evaluating the action of this operator on the basis states: for $|0\\rangle$ we have:\n",
    "\n",
    "\n",
    "$$ 2|0\\rangle\\langle 0|0\\rangle - I|0\\rangle = 2 |0\\rangle - |0\\rangle = |0\\rangle,$$\n",
    "\n",
    "\n",
    "and for any other state $|x\\rangle$ we have\n",
    "\n",
    "\n",
    "$$ 2|0\\rangle\\langle 0|x\\rangle - I|x\\rangle = 2 |0\\rangle \\langle 0|x\\rangle - |x\\rangle = -|x\\rangle.$$\n",
    "\n",
    "Moreover, the steps 2, 3 and 4 taken together can be written similarly in terms of $|\\psi\\rangle = H^{\\otimes n} |0\\rangle$ as \n",
    "\n",
    "\n",
    "$$ 2|\\psi\\rangle\\langle \\psi| - I.$$\n",
    "\n",
    "This means that the overall effect of the Grover iteration $G$ is \n",
    "\n",
    "$$ G = (2|\\psi\\rangle\\langle \\psi| - I)O. $$\n",
    "\n",
    "We will show that a single Grover iteration amounts to a simple rotation in the 2D space spanned by the state uniform superposition state $|\\psi\\rangle$ and the solution state.\n",
    "\n",
    "Consider the state \n",
    "\n",
    "$$|l\\rangle = \\frac{1}{\\sqrt{N-1}} \\sum_{x \\neq k} |x \\rangle, $$\n",
    "\n",
    "which is a uniform superposition of all non-solution states.\n",
    "\n",
    "Clearly, our initial state  $|\\psi\\rangle$ can be written as a superposition of $|l\\rangle $ and the solution state $|k\\rangle $ as follows\n",
    "\n",
    "$$|\\psi\\rangle = \\sqrt{\\frac{N-1}{N}}|l\\rangle + \\sqrt{\\frac{1}{N}}|k\\rangle. $$\n",
    "\n",
    "Hence the algorithm is intialized in a vector in the plane spanned by $|l\\rangle$ and $|k\\rangle$. We can also see that the action of $O$ on this plane is simply a **reflection** about the vector representing the state $l\\rangle$, since \n",
    "\n",
    "$$ O |l\\rangle  = |l\\rangle, \\quad O |k\\rangle =-|k\\rangle.   $$\n",
    "\n",
    "We have also seen already that $(2|\\psi\\rangle\\langle \\psi| - I)$ is a reflection through $|\\psi\\rangle$. As $G$ is the product of two reflections, it must be a rotation. Each Grover iteration rotates the register through an angle\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Part I. Oracles for Grover's Search**\n",
    "    \n",
    "    // Task 1.1. The |11...1⟩ oracle\n",
    "    // Inputs:\n",
    "    //      1) N qubits in an arbitrary state |x⟩ (input/query register)\n",
    "    //      2) a qubit in an arbitrary state |y⟩ (target qubit)\n",
    "    // Goal: Flip the state of the target qubit (i.e., apply an X gate to it)\n",
    "    //       if the query register is in the |11...1⟩ state,\n",
    "    //       and leave it unchanged if the query register is in any other state.\n",
    "    //       Leave the query register in the same state it started in.\n",
    "    // Example:\n",
    "    //       If the query register is in state |00...0⟩, leave the target qubit unchanged.\n",
    "    //       If the query register is in state |10...0⟩, leave the target qubit unchanged.\n",
    "    //       If the query register is in state |11...1⟩, flip the target qubit.\n",
    "    //       If the query register is in state (|00...0⟩ + |11...1⟩) / sqrt(2), and the target is in state |0⟩,\n",
    "    //       the joint state of the query register and the target qubit should be (|00...00⟩ + |11...11⟩) / sqrt(2).\n",
    "   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"Microsoft.Quantum.Standard::0.6.1905.301\",\"Microsoft.Quantum.Katas::0.6.1905.301\"]",
      "text/html": [
       "<ul><li>Microsoft.Quantum.Standard::0.6.1905.301</li><li>Microsoft.Quantum.Katas::0.6.1905.301</li></ul>"
      ],
      "text/plain": [
       "Microsoft.Quantum.Standard::0.6.1905.301, Microsoft.Quantum.Katas::0.6.1905.301"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%package Microsoft.Quantum.Katas"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "\"Success!\"",
      "text/plain": [
       "Success!"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%kataa T11_Oracle_AllOnes_Test\n",
    " operation Oracle_AllOnes (queryRegister : Qubit[], target : Qubit) : Unit {\n",
    "        \n",
    "        body (...) {\n",
    "            Controlled X(queryRegister,target);\n",
    "        }\n",
    "        \n",
    "        adjoint self;\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task 1.2. The $|1010...⟩$ oracle**\n",
    "\n",
    "Inputs:\n",
    "1. N qubits in an arbitrary state |x⟩ (input/query register)\n",
    "2. a qubit in an arbitrary state |y⟩ (target qubit)\n",
    "\n",
    "\n",
    "Goal:  Flip the state of the target qubit if the query register is in the |1010...⟩ state; that is, the state with alternating 1 and 0 values, with any number of qubits in the register. Leave the state of the target qubit unchanged if the query register is in any other state. Leave the query register in the same state it started in.\n",
    "            \n",
    "Example:\n",
    "If the register is in state |0000000⟩, leave the target qubit unchanged.\n",
    "If the register is in state |10101⟩, flip the target qubit.\n",
    "   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "\"Success!\"",
      "text/plain": [
       "Success!"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%kata T12_Oracle_AlternatingBits_Test\n",
    "operation Oracle_AlternatingBits (queryRegister : Qubit[], target : Qubit) : Unit {\n",
    "        \n",
    "        body (...) {\n",
    "            let queryLength = Length(queryRegister);\n",
    "            for(idx in 1..2..queryLength-1){\n",
    "                X(queryRegister[idx]);\n",
    "            }\n",
    "            Controlled X(queryRegister,target);\n",
    "            for(idx in 1..2..queryLength-1){\n",
    "                X(queryRegister[idx]);\n",
    "            }\n",
    "\n",
    "        } \n",
    "        \n",
    "        adjoint self;\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task 1.3. Arbitrary bit pattern oracle**\n",
    "\n",
    "Inputs:\n",
    "\n",
    "1. N qubits in an arbitrary state |x⟩ (input/query register)\n",
    "2. a qubit in an arbitrary state |y⟩ (target qubit)\n",
    "3. a bit pattern of length N represented as Bool[]\n",
    "\n",
    " Flip the state of the target qubit if the query register is in the state described by the given bit pattern (true represents qubit state One, and false represents Zero). Leave the state of the target qubit unchanged if the query register is in any other state. Leave the query register in the same state it started in.\n",
    "\n",
    "Example: If the bit patterns is [true, false], you need to flip the target qubit if and only if the qubits are in the |10⟩ state.\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n"
     ]
    },
    {
     "data": {
      "application/json": "\"Success!\"",
      "text/plain": [
       "Success!"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%kata T13_Oracle_ArbitraryPattern_Test\n",
    "operation Oracle_ArbitraryPattern (queryRegister : Qubit[], target : Qubit, pattern : Bool[]) : Unit {\n",
    "        \n",
    "        body (...) {\n",
    "            // The following line enforces the constraint on the input arrays.\n",
    "            // You don't need to modify it. Feel free to remove it, this won't cause your code to fail.\n",
    "            //EqualityFactI(Length(queryRegister), Length(pattern), \"Arrays should have the same length\");\n",
    "\n",
    "            let queryLength = Length(queryRegister);\n",
    "            for(idx in 0..queryLength-1){\n",
    "                if(pattern[idx] == false) {\n",
    "                    X(queryRegister[idx]);\n",
    "                }\n",
    "            }\n",
    "            Controlled X(queryRegister,target);\n",
    "            for(idx in 0..queryLength-1){\n",
    "                if(pattern[idx] == false) {\n",
    "                    X(queryRegister[idx]);\n",
    "                }\n",
    "            }\n",
    "        }\n",
    "        \n",
    "        adjoint self;\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task 1.4*. Oracle converter**\n",
    "\n",
    "Input:  A marking oracle: an oracle that takes a register and a target qubit and flips the target qubit if the register satisfies a certain condition\n",
    "\n",
    "Output: A phase-flipping oracle: an oracle that takes a register and flips the phase of the register if it satisfies this condition\n",
    "    \n",
    "     Note: Grover's algorithm relies on the search condition implemented as a phase-flipping oracle,\n",
    "     but it is often easier to write a marking oracle for a given condition. This transformation\n",
    "     allows to convert one type of oracle into the other. The transformation is described at\n",
    "     https://en.wikipedia.org/wiki/Grover%27s_algorithm, section \"Description of Uω\".\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Expecting only one Q# operation in code. Using the first one\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n"
     ]
    },
    {
     "data": {
      "application/json": "\"Success!\"",
      "text/plain": [
       "Success!"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%kata T14_OracleConverter_Test\n",
    "\n",
    "operation OracleConverterImpl (markingOracle : ((Qubit[], Qubit) => Unit is Adj), register : Qubit[]) : Unit\n",
    "    is Adj {\n",
    "        \n",
    "        using (target = Qubit()) {\n",
    "            // Put the target into the |-⟩ state\n",
    "            X(target);\n",
    "            H(target);\n",
    "                \n",
    "            // Apply the marking oracle; since the target is in the |-⟩ state,\n",
    "            // flipping the target if the register satisfies the oracle condition will apply a -1 factor to the state\n",
    "            markingOracle(register, target);\n",
    "                \n",
    "            // Put the target back into |0⟩ so we can return it\n",
    "            H(target);\n",
    "            X(target);\n",
    "        }\n",
    "    }\n",
    "    \n",
    "    \n",
    "    function OracleConverter (markingOracle : ((Qubit[], Qubit) => Unit is Adj)) : (Qubit[] => Unit is Adj) {\n",
    "        return OracleConverterImpl(markingOracle, _);\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Part II. The Grover iteration**\n",
    "    \n",
    "Task 2.1. The Hadamard transform\n",
    "\n",
    "Input: A register of N qubits in an arbitrary state\n",
    "Goal:  Apply the Hadamard transform to each of the qubits in the register.\n",
    "\n",
    "Note:  If the register started in the |0...0⟩ state, this operation will prepare an equal superposition of all 2^N basis states."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "\"Success!\"",
      "text/plain": [
       "Success!"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%kata T21_HadamardTransform_Test\n",
    " operation HadamardTransform (register : Qubit[]) : Unit\n",
    "    is Adj {\n",
    "        let N = Length(register);\n",
    "        for(idx in 0..N-1){\n",
    "            H(register[idx]);\n",
    "        } \n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task 2.2. Conditional phase flip**\n",
    "\n",
    "Input: A register of N qubits in an arbitrary state.\n",
    "\n",
    "Goal:  Flip the sign of the state of the register if it is not in the |0...0⟩ state.\n",
    "\n",
    "Example:\n",
    "\n",
    "If the register is in state |0...0⟩, leave it unchanged.\n",
    "If the register is in any other basis state, multiply its phase by -1.\n",
    "Note: This operation implements operator 2|0...0⟩⟨0...0| - I."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "\"Success!\"",
      "text/plain": [
       "Success!"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%kata T22_ConditionalPhaseFlip_Test\n",
    "operation ConditionalPhaseFlip (register : Qubit[]) : Unit\n",
    "    is Adj {\n",
    "    \n",
    "        \n",
    "        // Hint 1: Note that quantum states are defined up to a global phase.\n",
    "        // Thus the state obtained as a result of this operation is the same\n",
    "        // as the state obtained by flipping the sign of only the |0...0⟩ state.\n",
    "            \n",
    "        // Hint 2: You can use the same trick as in the oracle converter task.\n",
    "            \n",
    "        // ...\n",
    "        \n",
    "        let N = Length(register);\n",
    "\n",
    "        using (target = Qubit()) {\n",
    "            // Put the target into the |-⟩ state\n",
    "            X(target);\n",
    "            H(target);\n",
    "                \n",
    "            // Apply the marking oracle; since the target is in the |-⟩ state,\n",
    "            // flipping the target if the register satisfies the oracle condition will apply a -1 factor to the state\n",
    "            for(idx in 0..N-1){\n",
    "                X(register[idx]);\n",
    "            } \n",
    "            Controlled X(register,target);\n",
    "            for(idx in 0..N-1){\n",
    "                X(register[idx]);\n",
    "            } \n",
    "                \n",
    "            // Put the target back into |0⟩ so we can return it\n",
    "            H(target);\n",
    "            X(target);\n",
    "        }\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task 2.3. The Grover iteration**\n",
    "\n",
    "Inputs:\n",
    "1. N qubits in an arbitrary state |x⟩ (input/query register)\n",
    "2. a phase-flipping oracle that takes an N-qubit register and flips the phase of the state if the register is in the desired state.\n",
    "\n",
    "Goal:  Perform one Grover iteration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n"
     ]
    },
    {
     "data": {
      "application/json": "\"Success!\"",
      "text/plain": [
       "Success!"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%kata T23_GroverIteration_Test\n",
    "\n",
    "operation GroverIteration (register : Qubit[], oracle : (Qubit[] => Unit is Adj)) : Unit\n",
    "    is Adj {\n",
    "        \n",
    "        // Hint: A Grover iteration consists of 4 steps:\n",
    "        //    1) apply the oracle\n",
    "        oracle(register);\n",
    "        //    2) apply the Hadamard transform\n",
    "        HadamardTransform(register);\n",
    "        //    3) perform a conditional phase shift\n",
    "        ConditionalPhaseFlip(register);\n",
    "        //    4) apply the Hadamard transform again\n",
    "        HadamardTransform(register);\n",
    "        \n",
    "\n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Part III. Putting it all together: Grover's search algorithm**\n",
    "    \n",
    "**Task 3.1. Grover's search**\n",
    "\n",
    "Inputs:\n",
    "1. N qubits in the |0...0⟩ state,\n",
    "2. a marking oracle, and\n",
    "3. the number of Grover iterations to perform.\n",
    "\n",
    "Goal: Use Grover's algorithm to leave the register in the state that is marked by the oracle as the answer \n",
    "with high probability.\n",
    "\n",
    "Note: The number of iterations is passed as a parameter because it is defined by the nature of the problem\n",
    "and is easier to configure/calculate outside the search algorithm itself (for example, in the driver)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n",
      "[WARNING] The callable Microsoft.Quantum.Canon.BoolArrFromPositiveInt has been deprecated in favor of Microsoft.Quantum.Convert.IntAsBoolArray.\n"
     ]
    },
    {
     "data": {
      "application/json": "\"Success!\"",
      "text/plain": [
       "Success!"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%kata T31_GroversSearch_Test\n",
    "\n",
    "operation GroversSearch (register : Qubit[], oracle : ((Qubit[], Qubit) => Unit is Adj), iterations : Int) : Unit {\n",
    "       \n",
    "        let flipOracle_general=OracleConverter(oracle);\n",
    "        HadamardTransform(register);\n",
    "\n",
    "        for(i in 1..iterations){\n",
    "            GroverIteration(register,flipOracle_general);\n",
    "           \n",
    "        }\n",
    "   \n",
    "    }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, the following code shows the method at work for $N=4$. Feel free to choose your own pattern. Longer patterns will require adjustment of the number of iterations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"GroverWrapper\"]",
      "text/html": [
       "<ul><li>GroverWrapper</li></ul>"
      ],
      "text/plain": [
       "GroverWrapper"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "open Microsoft.Quantum.Characterization;\n",
    "\n",
    "operation GroverWrapper(): Int[] {\n",
    "    let pattern = [false,false,true,false,false,false,false,false];\n",
    "    let markingOracle = Oracle_ArbitraryPattern(_, _, pattern);\n",
    "    let n = Length(pattern);\n",
    "    mutable output = new Int[n];\n",
    "    mutable test = new Bool[][1];\n",
    "    \n",
    "    let nSamples=100;\n",
    "    \n",
    "    for(j in 1..nSamples){\n",
    "        using(register=Qubit[n]){\n",
    "            GroversSearch(register, markingOracle, 10);\n",
    "            //let rawoutput = MeasureAllZ(register);\n",
    "            for(i in 0..n-1){\n",
    "                if(M(register[i])==One){\n",
    "                set output w/= i <- output[i]+1;\n",
    "                }\n",
    "            }\n",
    "            \n",
    "            ResetAll(register);\n",
    "        }\n",
    "\n",
    "    }\n",
    "    return output;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[3,3,98,3,3,1,2,4]",
      "text/html": [
       "<ul><li>3</li><li>3</li><li>98</li><li>3</li><li>3</li><li>1</li><li>2</li><li>4</li></ul>"
      ],
      "text/plain": [
       "3, 3, 98, 3, 3, 1, 2, 4"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%simulate GroverWrapper"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Q#",
   "language": "qsharp",
   "name": "iqsharp"
  },
  "language_info": {
   "file_extension": ".qs",
   "mimetype": "text/x-qsharp",
   "name": "qsharp",
   "version": "0.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
