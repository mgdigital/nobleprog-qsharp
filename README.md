# Instructions:

- Install Docker
- Git clone this repository (press clone button on top right of main page)
- From within the cloned folder, run `docker-compose up`
