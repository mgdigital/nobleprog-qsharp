{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Quantum Key Distribution\n",
    "\n",
    "\n",
    "## Introduction\n",
    "\n",
    "Consider a simple communications situation, in which a sender, **Alice**,  wants to deliver a secret message to a recipient, **Bob**. **Eve**, an eavesdropper, is interested in intercepting the message. Alice takes the **plaintext**, the unencrypted information, and produces the **ciphertext**  using a **cryptographic key**. \n",
    "\n",
    "This key is a string of bits that is applied to the plaintext using a mathematical function, such as the bitwise XOR or a transposition/substitution map etc. The encrypted message is sent over a channel and delivered to Bob, who also possesses the same cryptographic key and performs the inverse operation, he applies the key to the ciphertext to recover the plaintext. \n",
    "\n",
    "In the case in which they have pre-shared the **same** key, called **symmetric cryptography**, this is the only element that Eve, who has intercepted only the ciphertext, needs to retrieve the plaintext message. Simple ciphers, such as transposition and substitution maps are vulnerable, because statistical properties of the underlying language can be used to infer the plaintext. To ensure perfect secrecy one would have to use a completely random generated key, as long as the plaintext, and never use it again. This approach is the so-called **one-time pad** (OTP). Once this random key is generated, Alice can apply for example the bitwise XOR to the plaintext and, as the XOR is symmetrical, Bob just needs to reapply the XOR with the same key to the ciphertext to obtain the plaintext. \n",
    "\n",
    "## Quantum cryptography\n",
    "\n",
    "Suppose that *Alice* and *Bob* are connected via a **quantum channel**, which they can use to exchange qubits. This channel is not used directly to send a private message, but only to exchange random qubits that after processing will compose the encryption key.  If key sharing is completed successfully, this key can be used as a **one-time pad**  to produce an encrypted message that can be be delivered over a **classical channel** using symmetrical cryptography. The key should be completely random, as long as the message, and discarded after use; the procedure can be repeated for every message that needs to be delivered. \n",
    "\n",
    "More specifically *Alice* produces an **initial key**, selecting a sequence of **random bits**, '$0$' and '$1$', and picking a sequence of **eigenstates**, with respect to a randomly chosen basis between: the standard Z basis $\\{| 0 \\rangle,\\  | 1 \\rangle\\}$ and the $X$ basis $\\{| + \\rangle,\\  | - \\rangle\\}$.\n",
    "\n",
    "*Alice* encodes the classical bits of the key one by one in a **qubit**, by preparing each qubit in an eigenstate of the basis chosen, so that only by measuring the qubits in the **right basis** one can retrieve with **certainty** the right classical bit, just as it happens with quantum money. In the meantime *Alice* keeps a note (in a **table**) of the basis that she has picked for every single qubit she has encoded.\n",
    "\n",
    "Now, using the quantum channel, she sends the stream of qubits to *Bob*, who is **unaware** of the basis used by *Alice* for the encoding. *Bob* receives these qubits prepared in a certain polarization eigenstate but he is unable to recognize which basis *Alice* used, because he cannot distinguish **non-orthogonal states** with a single measurement. Nonetheless he proceeds to measure each photon's state using a basis chosen randomly, and keeps a note of the measurement result and the associated basis that he used in a report table. \n",
    "\n",
    "Statistically, *Bob* will pick the correct basis  **$1/2$** of the time. When he measures using the right basis, he correctly retrieves the information bit of the key, but when he picks the wrong basis the information bit is not certain, since with respect to this basis, the qubit is in a **superposition** of the eigenstates of the correct basis, and it can collapse in either two of them with equal probability of **$1/2.$**\n",
    "\n",
    "Hence, *Alice* and *Bob* **sift** their key, i.e. they discard from the key all the bits obtained via measurements in the wrong basis, since they are not reliable. The price for this action is that the key will lose about **$1/2$** of its length, but the payoff is that they don't need to unveil their measurements, they just need to compare their tables, where they recorded the basis chosen, and they do that *after* the measurement has occurred.\n",
    "\n",
    "So they open the **classical channel** and only now *Alice* tells (publicly) *Bob* which basis she used to encode the key; they **compare** the **tables** and discard the bits obtained measuring qubits in different basis. What they obtain is a perfectly correlate **sifted key**, the same for both of them, ready for use. This key can be employed as a one-time pad and once is used up completely, the procedure can be repeated again to produce a new random key. \n",
    "\n",
    "What happens if we now introduce an **eavesdropper** in the communication? Suppose that *Eve* is able to intercept the qubits that Alice sends to Bob, and that she can also tap the classical communication channel. When she gets hold of the qubits she too is unaware which basis *Alice* used. She therefore guesses, and so will pick the wrong basis **$1/2$** of the time on average. If she measures in the wrong basis she has **$1/2$** probability to make the qubit collapse in the wrong eigenstate, so that on the whole she will have altered about **$1/4$** of the original qubits. This is the main difference with classical crypto: thanks to quantum mechanics observing implies measuring, and if this is not done accordingly, it changes the actual state (key).\n",
    "\n",
    "*Eve* produces a **candidate key** and passes on these (now altered) qubits to Bob who proceeds himself with his measurements. *Bob* constructs his table list of random basis and also obtains his candidate key, which will of course be different from *Eve*'s. When *Alice* broadcasts his basis table on the classical channel and *Bob* sift his  key accordingly, he will obtain a key different from *Alice*'s, unusable, since even in the same basis choice qubits will be different about **$1/4$** of the times. If *Alice* try to encrypt a message, symmetrical cryptography would fail and both *Alice* and *Bob* will know that communication has been compromised. \n",
    "\n",
    "If *Alice* and *Bob* never compare their measurement and they only compare basis tables they have no way of knowing that the state has been altered, until the encrypted message is produced, sent and decryption fails. However they can decide to initiate **key sharing** by also comparing their measurement on a certain number of qubits, and, only when they are convinced that the channel is free of interference, they proceed with the actual key sharing. Of course the part of the key that represents the unveiled measurement has to be discarded from it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The BB84 Protocol for Quantum Key Distribution\n",
    "\n",
    "Suppose Alice wants to agree a key of length $n$ with Bob. They can adopt the following quantum protocol.\n",
    "\n",
    "1. Alice chooses $(4 + \\delta)n$ data bits to send to Bob, where $\\delta$ is chosen to ensure that with high probability, sufficiently many bits are encoded using the same basis.\n",
    "2. Alice chooses a random $(4 + \\delta)n$-bit string $b$. She encodes each data bit as $\\{ |0\\rangle, |1\\rangle \\}$ if the corresponding bit of $b$ is 0 or $\\{ |+\\rangle, |-\\rangle\\}$ if $b$ is 1.\n",
    "3. Alice sends the resulting state to Bob.\n",
    "4. Bob receives the $(4 + \\delta)n$ qubits, announces this fact, and measures each qubit in the $X$ (with eigenstates $|+\\rangle$ and $|-\\rangle$) or $Z$ basis (with eigenstates $|0\\rangle$ and $|1\\rangle$) at random. \n",
    "5. Alice and Bob compare bases, and discard those bits where Bob measured in a different basis from Alice. With high probability, there are at least $2n$ bits left (if not, restart the protocol). They keep $2n$ bits.\n",
    "6. Alice selects a random subset of $n$ bits that will serve as a check on Eve's interference, and tells Bob which bits she selected.\n",
    "7. Alice and Bob announce and compare the values of the $n$ check bits. Assuming no interference or other error, these should agree perfectly. If they do not, they have detected interference.\n",
    "8. They use the remaining $n$ bits as a key with which to encrypt a message.\n",
    "\n",
    "A simplified version of this protocol is implemented as the operation `QKD` below. Note that we take $\\delta =0$ and simply use all bits to check for interference."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we define an operation that creates a random boolean array of the specified length - this can be used to generate a random message to transmit, and also determine the choice of basis for Alice, Bob and Eve."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"GenerateRandomBoolArray\"]",
      "text/html": [
       "<ul><li>GenerateRandomBoolArray</li></ul>"
      ],
      "text/plain": [
       "GenerateRandomBoolArray"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "open Microsoft.Quantum.Convert;\n",
    "open Microsoft.Quantum.Math;\n",
    "\n",
    "operation GenerateRandomBoolArray(n: Int): Bool[] {\n",
    "    return IntAsBoolArray(RandomIntPow2(n), n);\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we verify that this produces the expected output, defining a helper function that we can simulate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"BasisWrapper\"]",
      "text/html": [
       "<ul><li>BasisWrapper</li></ul>"
      ],
      "text/plain": [
       "BasisWrapper"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "operation BasisWrapper(): Bool[] {\n",
    "    return GenerateRandomBoolArray(5);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[false,true,true,true,false]",
      "text/html": [
       "<ul><li>False</li><li>True</li><li>True</li><li>True</li><li>False</li></ul>"
      ],
      "text/plain": [
       "False, True, True, True, False"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%simulate BasisWrapper"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now define an operation that Alice can use to encode her message into a qubit register, using the basis determined by the allocated random choice in `AliceBasis`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"AlicePrepare\"]",
      "text/html": [
       "<ul><li>AlicePrepare</li></ul>"
      ],
      "text/plain": [
       "AlicePrepare"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "operation AlicePrepare(AliceMessage: Bool[], AliceBasis: Bool[], register: Qubit[]): Unit {\n",
    "    let n = Length(AliceMessage);\n",
    "    for(i in 0..n-1){\n",
    "        if(AliceMessage[i]){\n",
    "                X(register[i]);\n",
    "            }\n",
    "        if(AliceBasis[i]){\n",
    "                H(register[i]);\n",
    "            }\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we define an operation that measures each entry of  `register` in the basis determined by `basis`, to be used by Bob and Eve, with their respective bases. It measures the ith entry in the `Z` basis (with eigenstates $|0\\rangle$ and $|1\\rangle$) if `basis[i]` is $1$ and in the `X` basis (with eigenstates  $|+\\rangle$ and $|-\\rangle$) otherwise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"MeasureInBasis\"]",
      "text/html": [
       "<ul><li>MeasureInBasis</li></ul>"
      ],
      "text/plain": [
       "MeasureInBasis"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "operation MeasureInBasis(basis: Bool[], register: Qubit[]): Result[] {\n",
    "        let n = Length(basis);\n",
    "        mutable output = new Result[n];\n",
    "        for(i in 0..n-1){\n",
    "\n",
    "            if(basis[i]){\n",
    "                H(register[i]);\n",
    "            }\n",
    "            set output w/= i <- M(register[i]);\n",
    "        }\n",
    "        return output;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next operation is used by Alice and Bob to find the positions in the register for which the basis Alice used to encode matches the one in which Bob made his measurement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"CheckBasesAgree\"]",
      "text/html": [
       "<ul><li>CheckBasesAgree</li></ul>"
      ],
      "text/plain": [
       "CheckBasesAgree"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "operation CheckBasesAgree(AliceBasis: Bool[], BobBasis: Bool[]): (Bool[],Int) {\n",
    "   let n = Length(AliceBasis);\n",
    "   mutable nAgree=0;\n",
    "   mutable BasisAgree = new Bool[n];\n",
    "\n",
    "        for(j in 0..n-1){\n",
    "            if(AliceBasis[j] == BobBasis[j]){\n",
    "                set BasisAgree w/= j <- true;\n",
    "                set nAgree+=1;\n",
    "            }\n",
    "    }\n",
    "    return (BasisAgree,nAgree);\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next operation is used to count the errors in the positions where Alice and Bob measured in the same basis. Any such errors are due to interference in the channel by Eve (assuming other sources of error are negligible)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"CountErrors\"]",
      "text/html": [
       "<ul><li>CountErrors</li></ul>"
      ],
      "text/plain": [
       "CountErrors"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "open Microsoft.Quantum.Convert;\n",
    "\n",
    "operation CountErrors(a: Bool[],Bob: Result[], Agreement: Bool[]): Int{\n",
    "    let nAgree = Length(Agreement);\n",
    "    mutable TransmissionErrors=0;\n",
    "    let BobBool=ResultArrayAsBoolArray(Bob);\n",
    "    for(i in 0..nAgree-1){\n",
    "            if(BobBool[i] != a[i] and Agreement[i] ){\n",
    "                      set TransmissionErrors +=1;\n",
    "            }\n",
    "    }\n",
    "    return TransmissionErrors;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The operation below simulates the protocol, for a message of length `n`. The `Eavesdropper` boolean flag determines whether or not Eve tries to intercept the message."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"QKD\"]",
      "text/html": [
       "<ul><li>QKD</li></ul>"
      ],
      "text/plain": [
       "QKD"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\n",
    "operation QKD(n: Int,Eavesdropper: Bool): (Int,Int) {\n",
    "\n",
    "\n",
    "    //Alice's boolean string to be transmitted\n",
    "    let a = GenerateRandomBoolArray(n);\n",
    "    \n",
    "    // Alice's encoding basis\n",
    "    let AliceBasis = GenerateRandomBoolArray(n);\n",
    "    \n",
    "     // Alice prepares her state\n",
    "    using(register=Qubit[n]){\n",
    "        \n",
    "        AlicePrepare(a,AliceBasis,register);\n",
    "        \n",
    "        // If Eve is listening\n",
    "        if(Eavesdropper){\n",
    "            \n",
    "            // Eve's basis \n",
    "            let EveBasis = GenerateRandomBoolArray(n);\n",
    "\n",
    "            //Eve measures all states;\n",
    "            let Eve = MeasureInBasis(EveBasis,register);\n",
    "        \n",
    "        }\n",
    "        \n",
    "    \n",
    "        // Bob's choice of basis\n",
    "        let BobBasis=GenerateRandomBoolArray(n);\n",
    "\n",
    "        // Bob measures all states\n",
    "        let Bob = MeasureInBasis(BobBasis,register);\n",
    "\n",
    "        // Alice and Bob determine the positions for which they used the same basis\n",
    "        let (AgreeBool,AgreeCount) = CheckBasesAgree(AliceBasis,BobBasis);\n",
    "\n",
    "        // Alice and Bob check whether their measurements agree on the states where they used the same basis\n",
    "        \n",
    "        let Errors = CountErrors(a,Bob,AgreeBool);\n",
    "\n",
    "        ResetAll(register);\n",
    "        \n",
    "        return (AgreeCount,Errors);\n",
    "\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we define a wrapper that simulates the protocol a large number of times, returning the average number of agreement positions and the number of errors detected. In any run where errors are present, Eve has been detected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"QKDWrapper\"]",
      "text/html": [
       "<ul><li>QKDWrapper</li></ul>"
      ],
      "text/plain": [
       "QKDWrapper"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "open Microsoft.Quantum.Convert;\n",
    "operation QKDWrapper(): (Double,Int) {\n",
    "    let nRuns=100;\n",
    "    mutable DetectionCount = 0;\n",
    "    mutable AgreeCount=0;\n",
    "    mutable Errors=0;\n",
    "    mutable Agreement=0;\n",
    "    for(i in 0..nRuns){\n",
    "        set (Agreement,Errors) = QKD(4,true);\n",
    "            if(Errors>0){\n",
    "                set DetectionCount+=1;\n",
    "            }\n",
    "        set AgreeCount+=Agreement;\n",
    "        }\n",
    "\n",
    "    let meanAgree = IntAsDouble(AgreeCount)/IntAsDouble(nRuns);\n",
    "    return (meanAgree,DetectionCount);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "{\"@type\":\"@tuple\",\"Item1\":1.96,\"Item2\":57}",
      "text/plain": [
       "(1.96, 57)"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%simulate QKDWrapper"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**To try:** Vary the length $n$ of Alice's encoded message - how long does it have to be before Eve is reliably detected?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Q#",
   "language": "qsharp",
   "name": "iqsharp"
  },
  "language_info": {
   "file_extension": ".qs",
   "mimetype": "text/x-qsharp",
   "name": "qsharp",
   "version": "0.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
