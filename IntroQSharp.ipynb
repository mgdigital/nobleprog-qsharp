{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introducing Q#\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Operations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An operation is the basic unit of quantum execution in Q#. It is roughly equivalent to a function in C or C++ or Python, or a static method in C# or Java.\n",
    "\n",
    "IQ#, the Q# Jupyter kernel, allows you to write Q# operations directly on a code cell and compile them by running the cell. Here is a very simple operation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"sayHello\"]",
      "text/html": [
       "<ul><li>sayHello</li></ul>"
      ],
      "text/plain": [
       "sayHello"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "operation sayHello() : Unit {\n",
    "    Message(\"Hello!\");\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you **Run** the cell, IQ# compiles the code and returns the name of the operations it found. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To execute operations, use the `%simulate` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello!\n"
     ]
    },
    {
     "data": {
      "application/json": "{\"@type\":\"tuple\"}",
      "text/plain": [
       "()"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%simulate sayHello"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This first operation did not take any arguments as input. It is straightforward to pass additional arguments as follows "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"HelloString\"]",
      "text/html": [
       "<ul><li>HelloString</li></ul>"
      ],
      "text/plain": [
       "HelloString"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "operation HelloString(greeting: String) : Unit {\n",
    "        \n",
    "        Message($\"Hello {greeting}!\");\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the IQ# `%simulate` method does not allow you to pass external arguments (this will not be a restriction in anything that follows). To execute the operation above, we use a simple wrapper operation.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"HelloWrapper\"]",
      "text/html": [
       "<ul><li>HelloWrapper</li></ul>"
      ],
      "text/plain": [
       "HelloWrapper"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "operation HelloWrapper() : Unit {\n",
    "\n",
    "    HelloString(\"Chris\");\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello Chris!\n"
     ]
    },
    {
     "data": {
      "application/json": "{\"@type\":\"tuple\"}",
      "text/plain": [
       "()"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%simulate HelloWrapper"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`%simulate` will print any console output on the notebook, and it will return the operation's return value. If the operation returns `Unit` it prints `()`, otherwise it prints the actual value.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Errors\n",
    "\n",
    "If the compiler detects any errors, it will instead show the list of errors in the output. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/snippet:(5,5): error QS5022: No identifier with that name exists.\n",
      "/snippet:(9,12): error QS6211: Unexpected argument tuple. Expecting an argument of type String.\n"
     ]
    }
   ],
   "source": [
    "operation InvalidQ() : Unit {\n",
    "\n",
    "    // The `FooBar` operation doesn't exist, so the following line\n",
    "    // will generate a `No variable with that name exists.` error:\n",
    "    FooBar(\"Hello again!\");\n",
    "    \n",
    "    // `Message` takes only one string argument, so the following line\n",
    "    // will generate a `Unexpected argument tuple.` error:\n",
    "    Message(1,2);\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise:** Fix the second error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Getting Help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Q# supports adding documentation to operations via comments in the code. When such documentation exists, you can access it from the notebook by adding a question mark before or after the operation name on a code cell, for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "{\"name\":\"Microsoft.Quantum.Intrinsic.X\",\"kind\":1,\"source\":\"D:/a/1/s/src/simulation/Intrinsic/Intrinsic.qs\",\"documentation\":\" # Summary\\n Applies the Pauli $X$ gate.\\n\\n \\\\begin{align}\\n     \\\\sigma_x \\\\mathrel{:=}\\n     \\\\begin{bmatrix}\\n         0 & 1 \\\\\\\\\\\\\\\\\\n         1 & 0\\n     \\\\end{bmatrix}.\\n \\\\end{align}\\n\\n # Input\\n ## qubit\\n Qubit to which the gate should be applied.\"}",
      "text/html": [
       "<h4><i class=\"fa fas fa-terminal\"></i> Microsoft.Quantum.Intrinsic.X <a href=\"D:/a/1/s/src/simulation/Intrinsic/Intrinsic.qs\"><i class=\"fa fas fa-code\"></i></a></h4><h1>Summary</h1>\n",
       "<p>Applies the Pauli $X$ gate.</p>\n",
       "<p>\\begin{align}\n",
       "\\sigma_x \\mathrel{:=}\n",
       "\\begin{bmatrix}\n",
       "0 &amp; 1 \\\\\n",
       "1 &amp; 0\n",
       "\\end{bmatrix}.\n",
       "\\end{align}</p>\n",
       "<h1>Input</h1>\n",
       "<h2>qubit</h2>\n",
       "<p>Qubit to which the gate should be applied.</p>\n"
      ],
      "text/plain": [
       "Microsoft.Quantum.Intrinsic.X"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Microsoft.Quantum.Intrinsic.X?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Namespaces\n",
    "\n",
    "Q# operations can call other operations previously defined; they can also call all the operations defined in standard namespaces such as [Microsoft.Quantum.Intrinsic](https://docs.microsoft.com/en-us/qsharp/api/qsharp/microsoft.quantum.intrinsic?view=qsharp-preview).\n",
    "\n",
    "For example, you can create a new operation that calls the previously compiled `HelloQ`:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"Quantum.Kata.PhaseEstimation.ArrayWrapperOperation1\",\"Quantum.Kata.PhaseEstimation.AssertEqualOnZeroState1\",\"Quantum.Kata.PhaseEstimation.AssertIsEigenstate\",\"Quantum.Kata.PhaseEstimation.AssertIsEigenstate_Reference\",\"Quantum.Kata.PhaseEstimation.Eigenstates_ZST\",\"Quantum.Kata.PhaseEstimation.Eigenstates_ZST_Reference\",\"Quantum.Kata.PhaseEstimation.GetMaxQubitCount\",\"Quantum.Kata.PhaseEstimation.GetOracleCallsCount\",\"Quantum.Kata.PhaseEstimation.Oracle_Reference\",\"Quantum.Kata.PhaseEstimation.QPE\",\"Quantum.Kata.PhaseEstimation.QPE_Reference\",\"Quantum.Kata.PhaseEstimation.ResetOracleCallsCount\",\"Quantum.Kata.PhaseEstimation.ResetQubitCount\",\"Quantum.Kata.PhaseEstimation.SingleBitPE\",\"Quantum.Kata.PhaseEstimation.SingleBitPE_Reference\",\"Quantum.Kata.PhaseEstimation.T11_Eigenstates_ZST_Test\",\"Quantum.Kata.PhaseEstimation.T12_UnitaryPower_Test\",\"Quantum.Kata.PhaseEstimation.T14_QPE_Test\",\"Quantum.Kata.PhaseEstimation.T15_E2E_QPE_Test\",\"Quantum.Kata.PhaseEstimation.T21_SingleBitPE_Test\",\"Quantum.Kata.PhaseEstimation.T22_TwoBitPE_Test\",\"Quantum.Kata.PhaseEstimation.Test1BitPEOnOnePair\",\"Quantum.Kata.PhaseEstimation.Test2BitPEOnOnePair\",\"Quantum.Kata.PhaseEstimation.TestAssertIsEigenstate_True\",\"Quantum.Kata.PhaseEstimation.TwoBitPE\",\"Quantum.Kata.PhaseEstimation.TwoBitPE_Reference\",\"Quantum.Kata.PhaseEstimation.UnitaryPower\",\"Quantum.Kata.PhaseEstimation.UnitaryPowerImpl_Reference\",\"Quantum.Kata.PhaseEstimation.UnitaryPower_Reference\",\"Quantum.Kata.QEC_BitFlipCode.AssertEqualOnZeroState\",\"Quantum.Kata.QEC_BitFlipCode.BindErrorCorrectionRound\",\"Quantum.Kata.QEC_BitFlipCode.BindErrorCorrectionRoundImpl\",\"Quantum.Kata.QEC_BitFlipCode.CorrectErrorOnAnyQubit\",\"Quantum.Kata.QEC_BitFlipCode.CorrectErrorOnAnyQubit_Reference\",\"Quantum.Kata.QEC_BitFlipCode.CorrectErrorOnLeftQubit\",\"Quantum.Kata.QEC_BitFlipCode.CorrectErrorOnLeftQubit_Reference\",\"Quantum.Kata.QEC_BitFlipCode.DetectErrorOnAnyQubit\",\"Quantum.Kata.QEC_BitFlipCode.DetectErrorOnAnyQubit_Reference\",\"Quantum.Kata.QEC_BitFlipCode.DetectErrorOnLeftQubit\",\"Quantum.Kata.QEC_BitFlipCode.DetectErrorOnLeftQubit_Reference\",\"Quantum.Kata.QEC_BitFlipCode.Encode\",\"Quantum.Kata.QEC_BitFlipCode.Encode_Reference\",\"Quantum.Kata.QEC_BitFlipCode.FindFirstDiff_Reference\",\"Quantum.Kata.QEC_BitFlipCode.IntToBoolArray\",\"Quantum.Kata.QEC_BitFlipCode.LogicalX\",\"Quantum.Kata.QEC_BitFlipCode.LogicalX_Reference\",\"Quantum.Kata.QEC_BitFlipCode.LogicalZ\",\"Quantum.Kata.QEC_BitFlipCode.LogicalZ_Reference\",\"Quantum.Kata.QEC_BitFlipCode.MeasureParity\",\"Quantum.Kata.QEC_BitFlipCode.MeasureParity_Reference\",\"Quantum.Kata.QEC_BitFlipCode.PauliErrors\",\"Quantum.Kata.QEC_BitFlipCode.StatePrep_Bitmask\",\"Quantum.Kata.QEC_BitFlipCode.StatePrep_Rotate\",\"Quantum.Kata.QEC_BitFlipCode.StatePrep_TwoBitmasks\",\"Quantum.Kata.QEC_BitFlipCode.StatePrep_WithError\",\"Quantum.Kata.QEC_BitFlipCode.T01_MeasureParity_Test\",\"Quantum.Kata.QEC_BitFlipCode.T02_Encode_Test\",\"Quantum.Kata.QEC_BitFlipCode.T03_DetectErrorOnLeftQubit_Test\",\"Quantum.Kata.QEC_BitFlipCode.T04_CorrectErrorOnLeftQubit_Test\",\"Quantum.Kata.QEC_BitFlipCode.T05_DetectErrorOnAnyQubit_Test\",\"Quantum.Kata.QEC_BitFlipCode.T06_CorrectErrorOnAnyQubit_Test\",\"Quantum.Kata.QEC_BitFlipCode.T07_LogicalX_Test\",\"Quantum.Kata.QEC_BitFlipCode.T08_LogicalZ_Test\",\"Quantum.Kata.QEC_BitFlipCode.TestParityOnState\",\"Quantum.Kata.QEC_BitFlipCode.ToString_Bitmask\"]",
      "text/html": [
       "<ul><li>Quantum.Kata.PhaseEstimation.ArrayWrapperOperation1</li><li>Quantum.Kata.PhaseEstimation.AssertEqualOnZeroState1</li><li>Quantum.Kata.PhaseEstimation.AssertIsEigenstate</li><li>Quantum.Kata.PhaseEstimation.AssertIsEigenstate_Reference</li><li>Quantum.Kata.PhaseEstimation.Eigenstates_ZST</li><li>Quantum.Kata.PhaseEstimation.Eigenstates_ZST_Reference</li><li>Quantum.Kata.PhaseEstimation.GetMaxQubitCount</li><li>Quantum.Kata.PhaseEstimation.GetOracleCallsCount</li><li>Quantum.Kata.PhaseEstimation.Oracle_Reference</li><li>Quantum.Kata.PhaseEstimation.QPE</li><li>Quantum.Kata.PhaseEstimation.QPE_Reference</li><li>Quantum.Kata.PhaseEstimation.ResetOracleCallsCount</li><li>Quantum.Kata.PhaseEstimation.ResetQubitCount</li><li>Quantum.Kata.PhaseEstimation.SingleBitPE</li><li>Quantum.Kata.PhaseEstimation.SingleBitPE_Reference</li><li>Quantum.Kata.PhaseEstimation.T11_Eigenstates_ZST_Test</li><li>Quantum.Kata.PhaseEstimation.T12_UnitaryPower_Test</li><li>Quantum.Kata.PhaseEstimation.T14_QPE_Test</li><li>Quantum.Kata.PhaseEstimation.T15_E2E_QPE_Test</li><li>Quantum.Kata.PhaseEstimation.T21_SingleBitPE_Test</li><li>Quantum.Kata.PhaseEstimation.T22_TwoBitPE_Test</li><li>Quantum.Kata.PhaseEstimation.Test1BitPEOnOnePair</li><li>Quantum.Kata.PhaseEstimation.Test2BitPEOnOnePair</li><li>Quantum.Kata.PhaseEstimation.TestAssertIsEigenstate_True</li><li>Quantum.Kata.PhaseEstimation.TwoBitPE</li><li>Quantum.Kata.PhaseEstimation.TwoBitPE_Reference</li><li>Quantum.Kata.PhaseEstimation.UnitaryPower</li><li>Quantum.Kata.PhaseEstimation.UnitaryPowerImpl_Reference</li><li>Quantum.Kata.PhaseEstimation.UnitaryPower_Reference</li><li>Quantum.Kata.QEC_BitFlipCode.AssertEqualOnZeroState</li><li>Quantum.Kata.QEC_BitFlipCode.BindErrorCorrectionRound</li><li>Quantum.Kata.QEC_BitFlipCode.BindErrorCorrectionRoundImpl</li><li>Quantum.Kata.QEC_BitFlipCode.CorrectErrorOnAnyQubit</li><li>Quantum.Kata.QEC_BitFlipCode.CorrectErrorOnAnyQubit_Reference</li><li>Quantum.Kata.QEC_BitFlipCode.CorrectErrorOnLeftQubit</li><li>Quantum.Kata.QEC_BitFlipCode.CorrectErrorOnLeftQubit_Reference</li><li>Quantum.Kata.QEC_BitFlipCode.DetectErrorOnAnyQubit</li><li>Quantum.Kata.QEC_BitFlipCode.DetectErrorOnAnyQubit_Reference</li><li>Quantum.Kata.QEC_BitFlipCode.DetectErrorOnLeftQubit</li><li>Quantum.Kata.QEC_BitFlipCode.DetectErrorOnLeftQubit_Reference</li><li>Quantum.Kata.QEC_BitFlipCode.Encode</li><li>Quantum.Kata.QEC_BitFlipCode.Encode_Reference</li><li>Quantum.Kata.QEC_BitFlipCode.FindFirstDiff_Reference</li><li>Quantum.Kata.QEC_BitFlipCode.IntToBoolArray</li><li>Quantum.Kata.QEC_BitFlipCode.LogicalX</li><li>Quantum.Kata.QEC_BitFlipCode.LogicalX_Reference</li><li>Quantum.Kata.QEC_BitFlipCode.LogicalZ</li><li>Quantum.Kata.QEC_BitFlipCode.LogicalZ_Reference</li><li>Quantum.Kata.QEC_BitFlipCode.MeasureParity</li><li>Quantum.Kata.QEC_BitFlipCode.MeasureParity_Reference</li><li>Quantum.Kata.QEC_BitFlipCode.PauliErrors</li><li>Quantum.Kata.QEC_BitFlipCode.StatePrep_Bitmask</li><li>Quantum.Kata.QEC_BitFlipCode.StatePrep_Rotate</li><li>Quantum.Kata.QEC_BitFlipCode.StatePrep_TwoBitmasks</li><li>Quantum.Kata.QEC_BitFlipCode.StatePrep_WithError</li><li>Quantum.Kata.QEC_BitFlipCode.T01_MeasureParity_Test</li><li>Quantum.Kata.QEC_BitFlipCode.T02_Encode_Test</li><li>Quantum.Kata.QEC_BitFlipCode.T03_DetectErrorOnLeftQubit_Test</li><li>Quantum.Kata.QEC_BitFlipCode.T04_CorrectErrorOnLeftQubit_Test</li><li>Quantum.Kata.QEC_BitFlipCode.T05_DetectErrorOnAnyQubit_Test</li><li>Quantum.Kata.QEC_BitFlipCode.T06_CorrectErrorOnAnyQubit_Test</li><li>Quantum.Kata.QEC_BitFlipCode.T07_LogicalX_Test</li><li>Quantum.Kata.QEC_BitFlipCode.T08_LogicalZ_Test</li><li>Quantum.Kata.QEC_BitFlipCode.TestParityOnState</li><li>Quantum.Kata.QEC_BitFlipCode.ToString_Bitmask</li></ul>"
      ],
      "text/plain": [
       "Quantum.Kata.PhaseEstimation.ArrayWrapperOperation1, Quantum.Kata.PhaseEstimation.AssertEqualOnZeroState1, Quantum.Kata.PhaseEstimation.AssertIsEigenstate, Quantum.Kata.PhaseEstimation.AssertIsEigenstate_Reference, Quantum.Kata.PhaseEstimation.Eigenstates_ZST, Quantum.Kata.PhaseEstimation.Eigenstates_ZST_Reference, Quantum.Kata.PhaseEstimation.GetMaxQubitCount, Quantum.Kata.PhaseEstimation.GetOracleCallsCount, Quantum.Kata.PhaseEstimation.Oracle_Reference, Quantum.Kata.PhaseEstimation.QPE, Quantum.Kata.PhaseEstimation.QPE_Reference, Quantum.Kata.PhaseEstimation.ResetOracleCallsCount, Quantum.Kata.PhaseEstimation.ResetQubitCount, Quantum.Kata.PhaseEstimation.SingleBitPE, Quantum.Kata.PhaseEstimation.SingleBitPE_Reference, Quantum.Kata.PhaseEstimation.T11_Eigenstates_ZST_Test, Quantum.Kata.PhaseEstimation.T12_UnitaryPower_Test, Quantum.Kata.PhaseEstimation.T14_QPE_Test, Quantum.Kata.PhaseEstimation.T15_E2E_QPE_Test, Quantum.Kata.PhaseEstimation.T21_SingleBitPE_Test, Quantum.Kata.PhaseEstimation.T22_TwoBitPE_Test, Quantum.Kata.PhaseEstimation.Test1BitPEOnOnePair, Quantum.Kata.PhaseEstimation.Test2BitPEOnOnePair, Quantum.Kata.PhaseEstimation.TestAssertIsEigenstate_True, Quantum.Kata.PhaseEstimation.TwoBitPE, Quantum.Kata.PhaseEstimation.TwoBitPE_Reference, Quantum.Kata.PhaseEstimation.UnitaryPower, Quantum.Kata.PhaseEstimation.UnitaryPowerImpl_Reference, Quantum.Kata.PhaseEstimation.UnitaryPower_Reference, Quantum.Kata.QEC_BitFlipCode.AssertEqualOnZeroState, Quantum.Kata.QEC_BitFlipCode.BindErrorCorrectionRound, Quantum.Kata.QEC_BitFlipCode.BindErrorCorrectionRoundImpl, Quantum.Kata.QEC_BitFlipCode.CorrectErrorOnAnyQubit, Quantum.Kata.QEC_BitFlipCode.CorrectErrorOnAnyQubit_Reference, Quantum.Kata.QEC_BitFlipCode.CorrectErrorOnLeftQubit, Quantum.Kata.QEC_BitFlipCode.CorrectErrorOnLeftQubit_Reference, Quantum.Kata.QEC_BitFlipCode.DetectErrorOnAnyQubit, Quantum.Kata.QEC_BitFlipCode.DetectErrorOnAnyQubit_Reference, Quantum.Kata.QEC_BitFlipCode.DetectErrorOnLeftQubit, Quantum.Kata.QEC_BitFlipCode.DetectErrorOnLeftQubit_Reference, Quantum.Kata.QEC_BitFlipCode.Encode, Quantum.Kata.QEC_BitFlipCode.Encode_Reference, Quantum.Kata.QEC_BitFlipCode.FindFirstDiff_Reference, Quantum.Kata.QEC_BitFlipCode.IntToBoolArray, Quantum.Kata.QEC_BitFlipCode.LogicalX, Quantum.Kata.QEC_BitFlipCode.LogicalX_Reference, Quantum.Kata.QEC_BitFlipCode.LogicalZ, Quantum.Kata.QEC_BitFlipCode.LogicalZ_Reference, Quantum.Kata.QEC_BitFlipCode.MeasureParity, Quantum.Kata.QEC_BitFlipCode.MeasureParity_Reference, Quantum.Kata.QEC_BitFlipCode.PauliErrors, Quantum.Kata.QEC_BitFlipCode.StatePrep_Bitmask, Quantum.Kata.QEC_BitFlipCode.StatePrep_Rotate, Quantum.Kata.QEC_BitFlipCode.StatePrep_TwoBitmasks, Quantum.Kata.QEC_BitFlipCode.StatePrep_WithError, Quantum.Kata.QEC_BitFlipCode.T01_MeasureParity_Test, Quantum.Kata.QEC_BitFlipCode.T02_Encode_Test, Quantum.Kata.QEC_BitFlipCode.T03_DetectErrorOnLeftQubit_Test, Quantum.Kata.QEC_BitFlipCode.T04_CorrectErrorOnLeftQubit_Test, Quantum.Kata.QEC_BitFlipCode.T05_DetectErrorOnAnyQubit_Test, Quantum.Kata.QEC_BitFlipCode.T06_CorrectErrorOnAnyQubit_Test, Quantum.Kata.QEC_BitFlipCode.T07_LogicalX_Test, Quantum.Kata.QEC_BitFlipCode.T08_LogicalZ_Test, Quantum.Kata.QEC_BitFlipCode.TestParityOnState, Quantum.Kata.QEC_BitFlipCode.ToString_Bitmask"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%workspace"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "They can also use all [Q# Prelude](https://docs.microsoft.com/en-us/qsharp/api/?view=qsharp-preview) operations defined in other namespaces by importing the namespace using the `open` statement. For example, to use [`PI`](https://docs.microsoft.com/en-us/qsharp/api/prelude/microsoft.quantum.extensions.math.pi?view=qsharp-preview) you would need to open the ` Microsoft.Quantum.Extensions.Math` namespace; optionally you can call the operation providing its fully qualified name, for example:\n",
    " "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"HelloPi\"]",
      "text/html": [
       "<ul><li>HelloPi</li></ul>"
      ],
      "text/plain": [
       "HelloPi"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "open Microsoft.Quantum.Math;\n",
    "open Microsoft.Quantum.Convert;\n",
    "\n",
    "operation HelloPi() : Unit {\n",
    "    let pi = DoubleAsString(PI());\n",
    "    HelloString(pi);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello 3.14159265358979!\n"
     ]
    },
    {
     "data": {
      "application/json": "{\"@type\":\"tuple\"}",
      "text/plain": [
       "()"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%simulate HelloPi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"Microsoft.Quantum.Samples.IntegerFactorization.EstimatePeriod\",\"Microsoft.Quantum.Samples.IntegerFactorization.OrderFindingOracle\",\"Microsoft.Quantum.Samples.IntegerFactorization.Shor\",\"Microsoft.Quantum.Samples.IsMinus\",\"Microsoft.Quantum.Samples.IsPlus\"]",
      "text/html": [
       "<ul><li>Microsoft.Quantum.Samples.IntegerFactorization.EstimatePeriod</li><li>Microsoft.Quantum.Samples.IntegerFactorization.OrderFindingOracle</li><li>Microsoft.Quantum.Samples.IntegerFactorization.Shor</li><li>Microsoft.Quantum.Samples.IsMinus</li><li>Microsoft.Quantum.Samples.IsPlus</li></ul>"
      ],
      "text/plain": [
       "Microsoft.Quantum.Samples.IntegerFactorization.EstimatePeriod, Microsoft.Quantum.Samples.IntegerFactorization.OrderFindingOracle, Microsoft.Quantum.Samples.IntegerFactorization.Shor, Microsoft.Quantum.Samples.IsMinus, Microsoft.Quantum.Samples.IsPlus"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%workspace reload"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can define multiple operations in a single cell and use any valid Q# code, for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"PrepareRandomMessage\",\"SetMinus\",\"SetPlus\"]",
      "text/html": [
       "<ul><li>PrepareRandomMessage</li><li>SetMinus</li><li>SetPlus</li></ul>"
      ],
      "text/plain": [
       "PrepareRandomMessage, SetMinus, SetPlus"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "open Microsoft.Quantum.Math;\n",
    "\n",
    "/// # Summary\n",
    "/// Sets the qubit's state to |+⟩\n",
    "operation SetPlus(q: Qubit) : Unit {\n",
    "    Reset(q);\n",
    "    H(q);\n",
    "}\n",
    "\n",
    "/// # Summary\n",
    "/// Sets the qubit's state to |-⟩\n",
    "operation SetMinus(q: Qubit) : Unit {\n",
    "    Reset(q);\n",
    "    X(q);\n",
    "    H(q);\n",
    "}\n",
    "\n",
    "/// # Summary\n",
    "/// Randomly prepares the qubit into |+⟩ or |-⟩\n",
    "operation PrepareRandomMessage(q: Qubit) : Unit {\n",
    "\n",
    "    let choice = RandomInt(2);\n",
    "\n",
    "    if (choice == 0) {\n",
    "        Message(\"Prepared |-⟩\");\n",
    "        SetMinus(q);\n",
    "    } else {\n",
    "        Message(\"Prepared |+⟩\");\n",
    "        SetPlus(q);\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and create other operations that uses them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"TestPrepareQubits\"]",
      "text/html": [
       "<ul><li>TestPrepareQubits</li></ul>"
      ],
      "text/plain": [
       "TestPrepareQubits"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "open Microsoft.Quantum.Diagnostics;\n",
    "\n",
    "operation TestPrepareQubits() : Result {\n",
    "    mutable r = Zero;\n",
    "    \n",
    "    using (qubits = Qubit[5]) {\n",
    "        ApplyToEach(PrepareRandomMessage, qubits);\n",
    "        DumpMachine();\n",
    "        \n",
    "        set r = Measure([PauliX, PauliX, PauliX, PauliX, PauliX], qubits);\n",
    "        \n",
    "        ResetAll(qubits);\n",
    "    }\n",
    "    \n",
    "    return r;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Estimating resources\n",
    "\n",
    "The `%estimate` command lets you estimate the resources a given quantum operation will need to execute, without actually executing the operation. Similar to `%simulate` it takes the name of a no-arguments operation. However, `%estimate` does not keep track of the qubit's state and will not return the output of the operation, instead it returns the estimated values of how many resources, like Qubits and CNOT gates, the corresponding operation will use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "{\"CNOT\":0.0,\"QubitClifford\":10.0,\"R\":0.0,\"Measure\":11.0,\"T\":0.0,\"Depth\":0.0,\"Width\":5.0,\"BorrowedWidth\":0.0}",
      "text/html": [
       "<ul><li>[CNOT, 0]</li><li>[QubitClifford, 10]</li><li>[R, 0]</li><li>[Measure, 11]</li><li>[T, 0]</li><li>[Depth, 0]</li><li>[Width, 5]</li><li>[BorrowedWidth, 0]</li></ul>"
      ],
      "text/plain": [
       "[CNOT, 0], [QubitClifford, 10], [R, 0], [Measure, 11], [T, 0], [Depth, 0], [Width, 5], [BorrowedWidth, 0]"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%estimate TestPrepareQubits"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To learn more about resources estimation, take a look at [The ResourcesEstimator Target Machine](https://docs.microsoft.com/en-us/quantum/machines/resources-estimator?) documentation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Workspace\n",
    "\n",
    "The notebook uses the folder it lives on disk to define a workspace. It will try to compile all the Q# files (i.e. all files with a `.qs` extension) it finds under the current folder and will make the operations it finds available to operations in the notebook. For example, the [Operations.qs](/edit/Operations.qs) file in this folder defines two operations:\n",
    "* Microsoft.Quantum.Samples.IsMinus\n",
    "* Microsoft.Quantum.Samples.IsPlus\n",
    "\n",
    "To get the list of operations defined in the workspace, you can use the `%workspace` command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"Microsoft.Quantum.Samples.IntegerFactorization.EstimatePeriod\",\"Microsoft.Quantum.Samples.IntegerFactorization.OrderFindingOracle\",\"Microsoft.Quantum.Samples.IntegerFactorization.Shor\",\"Microsoft.Quantum.Samples.IsMinus\",\"Microsoft.Quantum.Samples.IsPlus\"]",
      "text/html": [
       "<ul><li>Microsoft.Quantum.Samples.IntegerFactorization.EstimatePeriod</li><li>Microsoft.Quantum.Samples.IntegerFactorization.OrderFindingOracle</li><li>Microsoft.Quantum.Samples.IntegerFactorization.Shor</li><li>Microsoft.Quantum.Samples.IsMinus</li><li>Microsoft.Quantum.Samples.IsPlus</li></ul>"
      ],
      "text/plain": [
       "Microsoft.Quantum.Samples.IntegerFactorization.EstimatePeriod, Microsoft.Quantum.Samples.IntegerFactorization.OrderFindingOracle, Microsoft.Quantum.Samples.IntegerFactorization.Shor, Microsoft.Quantum.Samples.IsMinus, Microsoft.Quantum.Samples.IsPlus"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%workspace"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These operations can be used in this notebook, for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"CheckPlus\"]",
      "text/html": [
       "<ul><li>CheckPlus</li></ul>"
      ],
      "text/plain": [
       "CheckPlus"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "open Microsoft.Quantum.Samples;\n",
    "\n",
    "operation CheckPlus() : Bool {\n",
    "    mutable result = false;\n",
    "    \n",
    "    using (q = Qubit()) {\n",
    "        SetPlus(q);\n",
    "        set result = IsPlus(q);\n",
    "        \n",
    "        Reset(q);\n",
    "    }\n",
    "\n",
    "    return result;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[WARNING] The callable Microsoft.Quantum.Primitive.Measure has been deprecated in favor of Microsoft.Quantum.Intrinsic.Measure.\n"
     ]
    },
    {
     "data": {
      "application/json": "true",
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%simulate CheckPlus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To pick up any changes you make to a Q# file in the workspace, use `%workspace reload`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": "[\"Microsoft.Quantum.Samples.IntegerFactorization.EstimatePeriod\",\"Microsoft.Quantum.Samples.IntegerFactorization.OrderFindingOracle\",\"Microsoft.Quantum.Samples.IntegerFactorization.Shor\",\"Microsoft.Quantum.Samples.IsMinus\",\"Microsoft.Quantum.Samples.IsPlus\"]",
      "text/html": [
       "<ul><li>Microsoft.Quantum.Samples.IntegerFactorization.EstimatePeriod</li><li>Microsoft.Quantum.Samples.IntegerFactorization.OrderFindingOracle</li><li>Microsoft.Quantum.Samples.IntegerFactorization.Shor</li><li>Microsoft.Quantum.Samples.IsMinus</li><li>Microsoft.Quantum.Samples.IsPlus</li></ul>"
      ],
      "text/plain": [
       "Microsoft.Quantum.Samples.IntegerFactorization.EstimatePeriod, Microsoft.Quantum.Samples.IntegerFactorization.OrderFindingOracle, Microsoft.Quantum.Samples.IntegerFactorization.Shor, Microsoft.Quantum.Samples.IsMinus, Microsoft.Quantum.Samples.IsPlus"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%workspace reload"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Other commands ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `%who`\n",
    "\n",
    "`%who` returns the list of all local and workspace operations available."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%who"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `%package`\n",
    "\n",
    "`%package` allows you to load nuget packages and makes available any Q# operations defined on them. For example, to use the operations from [Q#'s Quantum Chemistry Library](https://docs.microsoft.com/en-us/quantum/libraries/chemistry/?view=qsharp-preview), you must load the [Microsoft.Quantum.Chemistry](https://www.nuget.org/packages/Microsoft.Quantum.Chemistry/) nuget package:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%package Microsoft.Quantum.Chemistry"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`%package` returns the list of nuget packages currently loaded and their version."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `%version`\n",
    "\n",
    "`%version` simply returns the current versions of IQ# and of Jupyter Core (a library used by IQ#):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%version"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "open Microsoft.Quantum.Diagnostics;\n",
    "\n",
    "operation changeQubits() : Result {\n",
    "    mutable r = Zero;\n",
    "    \n",
    "    using (qubits = Qubit[1]) {\n",
    "        ApplyToEach(H, qubits);\n",
    "        DumpMachine();\n",
    "        \n",
    "        set r = Measure([PauliX], qubits);\n",
    "        \n",
    "        ResetAll(qubits);\n",
    "    }\n",
    "    \n",
    "    return r;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%simulate changeQubits"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Q#",
   "language": "qsharp",
   "name": "iqsharp"
  },
  "language_info": {
   "file_extension": ".qs",
   "mimetype": "text/x-qsharp",
   "name": "qsharp",
   "version": "0.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
